//##################### DATA ##################
var VARS =require('common/globals'); //Here we store all global variables / settings
if(VARS.displayAdMobAdsNavigation){var ADS=require('common/ads');} //AD LIBRARY
function ApplicationDash(sections)
{
	//create module instance
	var tgDashHolder = Ti.UI.createTabGroup();
	
	var self = Ti.UI.createWindow({
	    title:L('appname'),
		backgroundColor : VARS.windowBackgroundColor,
		navBarHidden : false,
		tabBarHidden : true,
		barColor : VARS.barColor,
		orientationModes : [Titanium.UI.PORTRAIT]
	});
	
	var tabMain = Ti.UI.createTab({
		window: self
	});
	
	//AD
	dashHeight="100%";
	if(VARS.displayAdMobAdsNavigation)
	{
		var theAd=ADS.requestAd({win:self});
		dashHeight=VARS._dpiHeight-VARS.AdHeight;
	}
	
	//Main Scroller
	var dashScroll=Ti.UI.createScrollView({
		top:0,
		width:"100%",
		height:dashHeight,
		contentHeight:"auto",
		contentWidth:"100%",
		backgroundColor:"#ededed",
	});
	
	//Data for knowing where we should put the elements
	var xBase=0;
	
	var y=0;
	var wHeight = Ti.Platform.displayCaps.platformHeight;
	var	wWidth = Ti.Platform.displayCaps.platformWidth;
	var tileWidth=159;
	var titleHeight=111;
	if(VARS._platform==VARS._iPad){tileWidth*=2;titleHeight*=2;xBase+=50;};
	var x=xBase;
	//Generate metro style dash items
	for(var i=0;i<sections.length;i++)
	{
		//Dash Holder
		dashHolderImage="/dash/dashMenuHolder.png";
		dWidth="160dp";
		dHeight="111dp";
		if(VARS._platform==VARS._iPad)
		{
			dashHolderImage="dash/ipad/dashMenuHolder.png";
			dWidth=320;
			dHeight=222;
			
		}
		var singleDash=Ti.UI.createView({
			top:y+"dp",
			left:x+"dp",
			//image:dashHolderImage,
			backgroundImage:dashHolderImage,
			win:sections[i].win,
			width:dWidth,
			height:dHeight,
		});
		sections[i].win.containingTab=tabMain;
		
		//Create Label
		dashNameBottom=20;
		if(VARS._platform==VARS._iPad)
		{
			dashNameBottom="30";
		}
		var dashName=Ti.UI.createLabel({
			text:sections[i].title,
			left:"0dp",
			textAlign:"center",
			bottom:dashNameBottom+"dp",
			font:VARS.h2,
			width:tileWidth+"dp",
			color:"#4e4d4d",
			touchEnabled:false,
		});
		singleDash.add(dashName);
		
		//The icon
		dashImageLocation="/dash/"+sections[i].icon;
		dashWidth="50dp";
		dashImageTop=15;
		if(VARS._platform==VARS._iPad)
		{
			dashImageLocation="dash/ipad/"+sections[i].icon;
			dashWidth=null;
			dashImageTop=25;
		}
		var dashoIcon=Ti.UI.createImageView({
			//left:"15dp",
			touchEnabled:false,
			top:dashImageTop+"dp",
			width:dashWidth,
			image:dashImageLocation,
		});
		singleDash.add(dashoIcon);
		
		singleDash.addEventListener('click',function(e)
		{
			tabMain.open(e.source.win);
		});	
		
		dashScroll.add(singleDash);
		
		//Recalculate new x,y
		if(((i+1)%2)==0)
		{
			x=xBase;
			y+=titleHeight;
		}
		else
		{
			x+=tileWidth+2;
		}
	}
	
	self.add(dashScroll);
	self.containingTab=tabMain;
	tgDashHolder.addTab(tabMain);
	if(VARS._platform==VARS._android){return self;}
	else {return tgDashHolder;}
	
}
module.exports = ApplicationDash;
