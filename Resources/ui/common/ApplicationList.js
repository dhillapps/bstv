//##################### DATA ##################
var VARS =require('common/globals'); //Here we store all global variables / settings
if(VARS.displayAdMobAdsNavigation){var ADS=require('common/ads');} //AD LIBRARY

function creteSimpleNavigationRow(p)
{
	//The Row
	var bgColor=VARS.rowColor;
	if(p.index%2==1){bgColor=VARS.rowOddColor;}
	var row = Ti.UI.createTableViewRow({
		height:"80dp",
		hasChild:VARS._platform!=VARS._android,
		backgroundColor:bgColor});
	row.className = 'datarow';
	row.clickName = 'row';
	row.index = p.index;
	row.data=p;
	row.win=p.win;
	
	
	//The left strip
	var strip=Ti.UI.createView({
		top:0,
		left:0,
		width:"5dp",
		height:"80dp",
		touchEnabled:false,
		backgroundColor:VARS.listColors[p.index%VARS.listColors.length],
	});
	row.add(strip);
	
	//The icon
	var listIcon=Ti.UI.createImageView({
		touchEnabled:false,
		left:"15dp",
		image:"/list/"+p.icon,
		width:"32dp",
		height:"32dp",
	});
	row.add(listIcon);
	
	
	//The title
	var textW="220dp";
	if(VARS._platform==VARS._iPad)
	{
		textW="668";
	}
	else if(VARS._platform==VARS._android)
	{
		textW="230dp";
	}
	var theTitle = Ti.UI.createLabel({
		text:p.title,
		left:"65dp",
		font:VARS.h1,
		width:textW,
		color:VARS.textColor,
		touchEnabled:false,
	});
	row.add(theTitle);
	
	return row;
}


function ApplicationList(sections) {
	//create module instance
	var tgListHolder = Ti.UI.createTabGroup();
	
	var self = Ti.UI.createWindow({
	    title:L('appname'),
		backgroundColor : VARS.windowBackgroundColor,
		navBarHidden : false,
		tabBarHidden : true,
		barColor : VARS.barColor,
		orientationModes : [Titanium.UI.PORTRAIT]
	});
	
	var tabMain = Ti.UI.createTab({
		window: self
	});
	
	
	//AD
	ListTableHeight="100%";
	if(VARS.displayAdMobAdsNavigation)
	{
		var theAd=ADS.requestAd({win:self});
		ListTableHeight=VARS._dpiHeight-VARS.AdHeight;
	}
	var ListTable=Ti.UI.createTableView({
		top:"0dp",
		width:"100%",
		height:ListTableHeight,
		backgroundColor : VARS.windowBackgroundColor,
		separatorColor : VARS.separatoColor,
	});
	var menuRows=[];
	
	
	
	for(var i=0;i<sections.length;i++)
	{
		var theRow=creteSimpleNavigationRow({
			title:sections[i].title,
			icon:sections[i].icon,
			index:i,
			win:sections[i].win,
		});	
		sections[i].win.containingTab=tabMain;
		theRow.addEventListener('click',function(e)
		{
			
			tabMain.open(e.source.win);
		});
		menuRows.push(theRow);
	}
	
	ListTable.setData(menuRows);
	self.add(ListTable);
	
	
	self.containingTab=tabMain;
	tgListHolder.addTab(tabMain);
	if(VARS._platform==VARS._android){return self;}
	else {return tgListHolder;}
};

module.exports = ApplicationList;
