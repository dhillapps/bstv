function ApplicationTabGroup(sections) {
	//create module instance
	var self = Ti.UI.createTabGroup({
		barColor:'#000'
	});
	
	for(var i=0;i<sections.length;i++)
	{
		if(!sections[i].url)
		{ 
			
		var theTab=Ti.UI.createTab({
			title: sections[i].title,
			icon: "tab/"+sections[i].icon,
			window: sections[i].win,
			
		});
		
		
		theTab.addEventListener('tab',function(e){
			
		});
		theTab.addEventListener('click',function(e){
		
		});
		theTab.addEventListener('touch',function(e){
	
		});
		}
		else
		{
			
			var theTab=Ti.UI.createTab({
			title: sections[i].title,
			icon: "tab/"+sections[i].icon,
			window: sections[i].win,
			url: sections[i].url,
			
		});
		}
		
		self.addTab(theTab);
		
		sections[i].win.containingTab = theTab;
		
	}
	
	return self;
};

module.exports = ApplicationTabGroup;
