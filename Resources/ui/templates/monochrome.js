exports.separatoColor="#E2E2E2";
exports.rowColor="#EEEEEE";
exports.rowOddColor="#EEEEEE";
exports.windowBackgroundColor="#E6E6E6";
exports.barColor="#9F9F9F";
exports.textColor="#3D3D3D";
exports.textSecondColor="#B2B2B2";
exports.listColors=["#fe4819","#d1005d","#0081ab","#009a3d"];

//Optional
exports.videosCoversColor="#F2F2F2";
exports.videosWindowBackgroundColor="#E6E6E6";
exports.videosTextColor="#6D6D6D";
