/**
 * This file is empty, no colors are set up, and the app loads the mobile OS default color values.
 */
//Optional - Contact Colors
exports.imageFrameColor="#E5E5E5";
exports.formTextColor="#0B0B0B";
exports.formBgColor="#E5E5E5";
exports.buttonColor="#676767";