var VARS =require('common/globals'); //Here we store all global variables / settings
function ApplicationWindow(title) {
	var self = Ti.UI.createWindow({
	    title:title,
		backgroundColor : VARS.windowBackgroundColor,
		navBarHidden : false,
		barColor : VARS.barColor,
		orientationModes : [Titanium.UI.PORTRAIT]
	});

	return self;
};

module.exports = ApplicationWindow;
