var VARS =require('common/globals'); //Here we store all global variables / settings
function ApplicationWebWindow(title,url) {
	var self = Ti.UI.createWindow({
	    title:title,
		backgroundColor : VARS.windowBackgroundColor,
		navBarHidden : false,
		barColor : VARS.barColor,
		orientationModes : [Titanium.UI.PORTRAIT]
	});
	
	var webview = Titanium.UI.createWebView({
		url : url,
	});
	self.add(webview);

	return self;
};

module.exports = ApplicationWebWindow;
