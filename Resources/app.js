/**
 * @author Daniel Dimov - NextWebArt  - dimovdaniel@yahoo.com
 * Titanium Blog App
 */

//##################### DATA ##################
var VARS =require('common/globals'); //Here we store all global variables / settings
var Window = require('ui/handheld/ApplicationWindow');
var WebWindow = require('ui/handheld/ApplicationWebWindow');
var Gallery=require('galleries_lib/galleryModule'); //Here are the functions to create the gallery
var News=require('news_lib/newsModule'); //Here are the functions to create news
var Contact=require('contact_lib/contactModule'); //Here are the functions to create the contacts
var Video=require('video_lib/videoModule'); //Here are the functions to create the videos
var Links=require('links_lib/linksModule'); //Here are the functions to create the links
var Sound=require('sound_lib/soundModule');
var About = require('about_lib/aboutModule');
var Storekit = require('ti.storekit');
var load = require('video_lib/load');
var indicator = load.createIndicatorWindow();

//For iPhone only include the push module
if(VARS._platform==VARS._iPhone&&VARS.useUrbanAirship)
{
	var Push=require('push_lib/pushModule'); //Here are the functions to create the links
	alert("UA");
	Push.registerDisplayUrbanAirshipPush({
		URBAN_AIRSHIP_APP_KEY:VARS.URBAN_AIRSHIP_APP_KEY,
		URBAN_AIRSHIP_MASTER_SECRET:VARS.URBAN_AIRSHIP_MASTER_SECRET
	});
}

if(VARS._displayAdMobAds)
{
	var Admob = require('ti.admob');
}


var openType=1;

//bootstrap and check dependencies
if (Ti.version < 1.8 ) {
	alert('Sorry - this application template requires Titanium Mobile SDK 1.8 or later');
}



//################### VIEWS & WINDOW ################
var galleryWindow = new Window(L('gallery'));
var gallerySection={
	win:galleryWindow,
	icon:'gallery.png',
    title:L('gallery'),
};

var newsWindow = new Window(L('news'));
var newsSection={
	win:newsWindow,
	icon:'news.png',
    title:L('news'),
};

var linkWindow = new Window(L('Social Media'));
var linkSection={
	win:linkWindow,
	icon:'link.png',
    title:L('Social'),
};

var videoSearchWindow = Ti.UI.createWindow({
	title:'Search Videos',
	navBarHidden:true
});
var videoSection={
	win:videoSearchWindow,
	icon:'magnify.png',
    title:L('Search'),
};

var videoWindow1 = new Window(L('Kid Snippets'));
var videoSection1={
	win:videoWindow1,
	icon:'balloon.png',
    title:L('Snippets'),
};

var videoWindow2 = new Window(L('Episodes'));
var videoSection2={
	win:videoWindow2,
	icon:'youtube.png',
    title:L('Episodes'),
};

var videoWindow3 = new Window(L('Kid Masterpieces'));
var videoSection3={
	win:videoWindow3,
	icon:'palette.png',
    title:L('Masterpieces'),
};

var videoWindow4 = new Window(L('Top 10'));
var videoSection4={
	win:videoWindow4,
	icon:'badge.png',
    title:L('Top 10'),
};

var videoWindow5 = new Window(L('New Videos'));
var videoSection5={
	win:videoWindow5,
	icon:'stats.png',
    title:L('New Videos'),
};




var contactWindow= new Window(L('Contact'));
var contactSection={
	win:contactWindow,
	icon:'contact.png',
    title:L('Contact'),
};

var aboutWindow= new Window(L('About'));
var aboutSection={
	win:aboutWindow,
	icon:'about.png',
    title:L('About'),
};

var soundWindow= new Window(L('Sound Board'));
var soundSection={
	win:soundWindow,
	icon:'sound-on.png',
	title:L('Sound Board'),
};

//Example how to display more web views
var storewindow = new Window(L('Store'));
var storesection={
	win:storewindow,
	icon:'shopping-cart.png',
    title:L('Store'),
};

//openWebBrowser(storewindow,"http://store.boredshorts.tv","test");

var webviewyt = Titanium.UI.createWebView({
	url: "http://store.boredshorts.tv",
    width: '100%',
    height:'100%',
    top:0,
    left:0,
    showScrollBars:false
	});
storewindow.add(webviewyt);

VARS.createWebViewButtons(webviewyt,"http://store.boredshorts.tv");
					
if(VARS._platform == VARS._iPad)
{
	videoSearchWindow.orientationModes = [Ti.UI.LANDSCAPE_LEFT,Ti.UI.LANDSCAPE_RIGHT];
	videoWindow1.orientationModes = [Ti.UI.LANDSCAPE_LEFT,Ti.UI.LANDSCAPE_RIGHT];
	videoWindow2.orientationModes = [Ti.UI.LANDSCAPE_LEFT,Ti.UI.LANDSCAPE_RIGHT];
	videoWindow3.orientationModes = [Ti.UI.LANDSCAPE_LEFT,Ti.UI.LANDSCAPE_RIGHT];
	videoWindow4.orientationModes = [Ti.UI.LANDSCAPE_LEFT,Ti.UI.LANDSCAPE_RIGHT];
	videoWindow5.orientationModes = [Ti.UI.LANDSCAPE_LEFT,Ti.UI.LANDSCAPE_RIGHT];
	soundWindow.orientationModes = [Ti.UI.LANDSCAPE_LEFT,Ti.UI.LANDSCAPE_RIGHT];

}	
var purchasesWin = new Window(L('Purchases'));
var tab1 = {
		title: L('Purchases'),
		icon: 'shopping-cart.png',
		url: 'purchases.js',
		win: purchasesWin
	};
//purchasesWin.containingTab = tab1;	


var restorepurchasesWin = new Window(L('Restore Purchase'));
var tab2 = {
		title: L('Restore Purchase'),
		url: 'purchases.js',
		icon:'dollar.png',
		win: restorepurchasesWin
	};


//######################## SECTION ORDER - CHANGE HERE #################
var sections=[];
sections.push(videoSection5);
sections.push(soundSection);
sections.push(videoSection1);
sections.push(videoSection);
sections.push(videoSection2);
sections.push(videoSection3);
sections.push(videoSection4);

//sections.push(newsSection);
//sections.push(gallerySection);
sections.push(storesection);
sections.push(linkSection);
sections.push(aboutSection);
sections.push(contactSection);
// sections.push(tab1);
sections.push(tab2);
//sections.push(aboutSectionClone);  <-- Example how to display more web view
//self.addTab(tab1);


//######################## INIT FUNCTION ###############################
// This is a single context application with mutliple windows in a stack
(function() {
	//determine platform and form factor and render approproate components
	var osname = Ti.Platform.osname,
		version = Ti.Platform.version,
		height = Ti.Platform.displayCaps.platformHeight,
		width = Ti.Platform.displayCaps.platformWidth;
	
	Ti.API.info("Height:"+height);
	Ti.API.info("\Width:"+width);
	//considering tablet to have one dimension over 900px - this is imperfect, so you should feel free to decide
	//yourself what you consider a tablet form factor for android
	var isTablet = osname === 'ipad' || (osname === 'android' && (width > 899 || height > 899));

    //Determine the Navigation type
    var navigationHandler=null;
  if(osname === 'android')
  {
  	navigationHandler= require('ui/common/ApplicationMetro');
  }
  else
  {
    if(VARS._NavigationType=="tab")
    {
    	navigationHandler= require('ui/common/ApplicationTabGroup');
    }
    else if(VARS._NavigationType=="list")
    {
    	navigationHandler= require('ui/common/ApplicationList');
    }
     else if(VARS._NavigationType=="metro")
    {
    	navigationHandler= require('ui/common/ApplicationMetro');
    }
     else if(VARS._NavigationType=="dash")
    {
    	navigationHandler= require('ui/common/ApplicationDash');
    }
  }
	new navigationHandler(sections).open();
})();


//######################## FUNCTION #####################################
//##################### VIDEO FUNCTION ###########
//Function to create YouTube Videos
createYouTubeVideos=function()
{
	Video.createVideoGrid({
			user_yt:VARS._YT_USERNAME,
			limit:VARS._VIDEO_COUNT,
			win:videoWindow,
		});
};
searchYouTubeVideos=function(searchTerm)
{
	Video.createVideoGrid({
			user_yt:VARS._YT_USERNAME,
			limit:VARS._VIDEO_COUNT,
			query:searchTerm,
			win:videoSearchWindow,
			searchbar:searchBar
	});

};
createYouTubePlaylist=function()
{
	Video1.createVideoGrid({
			user_yt:VARS._YT_USERNAME,
			limit:VARS._VIDEO_COUNT,
			playlist:'PLDFD0284A5DA24371',
			win:videoWindow1,
		});
};
createYouTubePlaylist2=function()
{
	Video2.createVideoGrid({
			user_yt:VARS._YT_USERNAME,
			limit:VARS._VIDEO_COUNT,
			playlist:'PL45D00A9F6BE69F91',
			win:videoWindow2,
		});
};

//Function to create Vimeo Videos
createVimeoVideos=function()
{
	Video.createVideoGrid({
			user_vimeo:VARS._VIMEO_USERNAME,
			limit:VARS._VIDEO_COUNT,
			win:videoWindow,
		});
};

//#################### CONTACT FUNCTION ###########
//Function to create contact
createContact=function()
{
	Contact.createContact({
		win:contactWindow,
		title:VARS.contactTitle,
		image:VARS.contactImage,
		imageTitle:VARS.contactImageTitle,
		imageDescription:VARS.contactImageDesc,
		infoText:VARS.contactInfoText,
		numLines:2, //Number of lines for the above text
		phoneNumber:VARS.contactNumber,
		web:VARS.contactSite,
		email:VARS.contactEmail,
		additionalParameters:{
			platform:"iPhone",
			version:"1.1",
		},
		//tab:tabContact
		});
};

//################ GALLERY FUNCTIONS ##############	
//Function to create NextGen Gallery
createNextGenGallery=function()
{
	
	Gallery.createGalleryList({
		win:galleryWindow,
		title:"NextGen Gallery",
		id:1,
		url:VARS._NextGen_Address,
		type:VARS._NEXT_GEN,
		openType: openType,
		//tab:tabGallery
		});
};

//Function to create Facebook Gallery
createFBGallery=function()
{

		Gallery.createGalleryList({
			win:galleryWindow,
			title:"Gallery",
			page:VARS._FBPageID,
			type:VARS._FB_GALLERY,
			openType: openType,
			//tab:tabGallery
		});
};

//Function to create Facebook Gallery
createPicasaGallery=function()
{
	
		Gallery.createGalleryList({
			win:galleryWindow,
			title:"Picasa Gallery",
			userID:VARS._PicasaUserId,
			type:VARS._PICASA,
			openType: openType,
			//tab:tabGallery
		});
};

//Function to create Flickr Gallery
createFlickrGallery=function()
{

		Gallery.createGalleryList({
			win:galleryWindow,
			title:"Flickr Gallery",
			type:VARS._FLICKR,
			openType: openType,
			//tab:tabGallery,
			appKEY:VARS._FlickrAppKEY,
			userID:VARS._FlickrUserID,
		});
};

//################ NEWS FUNCTIONS ################
//Function to create RSS
createRSSNews=function()
{
	News.createNewsList({
			type:VARS._RSS,
			url:VARS._RSS_URl,
			win:newsWindow,
			//tab:tab1,
	});
};

//Function to create JOOMLA
createJoomlaNews=function()
{
	News.createNewsList({
			type:VARS._JOOMLA,
			url:VARS._JOOMLA_URL,
			count:VARS._count,
			win:newsWindow,
			//tab:tab1,
	});
};

//Function to create WordPress News
createWordPressNews=function()
{
	News.createNewsList({
			type:VARS._WP,
			url:VARS._WordPress_URL,
			count:VARS._count,
			win:newsWindow,
			//tab:tab1,
	});
};

//Function to create Twitter News
createTweeterNews=function()
{
	News.createNewsList({
			type:VARS._TW,
			user:VARS._TWITTER_USER,
			win:newsWindow,
			//tab:tab1,
	});
};

createLinks=function()
{
	Links.createLinks({
		links:VARS._LINKS,
		win:linkWindow
	});
};

createSound=function()
{
	Sound.createSoundBoard({
		win:soundWindow
	});
};

//################ WEB FUNCTIONS #################
//Function to create about web section
createAboutWeb=function()
{
	About.createAbout({
		win:aboutWindow
	});
};

//Function to create the twitter web
createTwitterWeb=function()
{
	var webviewtw = Titanium.UI.createWebView({
		url : VARS._TW_URL,
	});
	twitterWindow.add(webviewtw);
};

//Function to create the facebook web
createFacebookWeb=function()
{
	
	var webviewfb = Titanium.UI.createWebView({
		url : VARS._FB_URL,
	});
	facebookWindow.add(webviewfb);
	if(VARS._platform!=VARS._android)
	{
		//This is in iOS
		
		//Create Refresh button
		var refreshFbButton=Ti.UI.createButton({
			systemButton:Titanium.UI.iPhone.SystemButton.REFRESH
		});
		facebookWindow.setRightNavButton(refreshFbButton);
		refreshFbButton.addEventListener('click',function(e){
			webviewfb.setUrl(VARS._FB_URL);
		});
		
	}
};

//######################### EVENT LISTENERS #####################
//Function that listens for open event on the GalleryWindow
galleryWindow.addEventListener('open',function(e)
{
	if(!VARS.galleryInitialized)
	{
		if(VARS._typeOfGallery==VARS._NEXT_GEN)
		{
			createNextGenGallery();
		}
		else if(VARS._typeOfGallery==VARS._FB_GALLERY)
		{
			createFBGallery();
		}
		else if(VARS._typeOfGallery==VARS._PICASA)
		{
			createPicasaGallery();
		}
		else if(VARS._typeOfGallery==VARS._FLICKR)
		{
			createFlickrGallery();
		}
		VARS.galleryInitialized=true;
	}
});


//Function that listens for open event on the NewsWindow
newsWindow.addEventListener('open',function(e)
{
	if(!VARS.newsInitialized)
	{
		if(VARS._typeOfNews==VARS._WP)
		{
			createWordPressNews();
		}
		else if(VARS._typeOfNews==VARS._RSS)
		{
			createRSSNews();
		}
		else if(VARS._typeOfNews==VARS._TW)
		{
			createTweeterNews();
		}
		else if(VARS._typeOfNews==VARS._JOOMLA)
		{
			createJoomlaNews();
		}
		
		VARS.newsInitialized=true;
	}
});

var searchBar = Ti.UI.createSearchBar({
	top:0,
	barColor:'#000',
	showCancel:false,
	height:'40'
});
if(Ti.Platform.osname == 'ipad' || (Ti.Platform.osname == 'android' && (VARS._dpiWidth > 899 || VARS._dpiHeight > 899)))
{
	var image = Ti.UI.createImageView({
		top:'60',
		image:'searchImageiPadmini.png'
	});
}
else
{
	var image = Ti.UI.createImageView({
		top:'60',
		image:'searchImageiPhone.png'
	});
}

videoSearchWindow.add(searchBar);
videoSearchWindow.add(image);
//videoSearchWindow.backgroundColor = "#fff";
searchBar.addEventListener('return', function(e) {
	
	var network = Ti.Network;
	if(network.online)
	{
			searchYouTubeVideos(e.value);
			searchBar.blur();

	}
	else
	{
		Ti.UI.createAlertDialog({
			title:'No Internet Connection',
			message:'Please connect to the Internet',
			buttonNames:['Close']
		}).show();
		
	}
	
});

videoSearchWindow.addEventListener('click', function(e) {
	searchBar.blur();
});


//Function that listens for focus on video window
videoWindow1.addEventListener('focus',function(e)
{
	var network = Ti.Network;
	
	if(network.online)
	{
		if(!VARS.video1Initialized)
		{
			Video.createVideoGrid({
				user_yt:VARS._YT_USERNAME,
				limit:VARS._VIDEO_COUNT,
				playlist:'PLDFD0284A5DA24371',
				win:videoWindow1,
			});
			VARS.video1Initialized=true;
		}
	}
	else if (!VARS.video1Initialized)
	{
		Ti.UI.createAlertDialog({
			title:'No Internet Connection',
			message:'Please connect to the Internet',
			buttonNames:['Close']
		}).show();
		
		var button1 = Ti.UI.createButton({
			title:'Refresh',
			width:100,
			height:30
		});
		 videoWindow1.add(button1);
		button1.addEventListener('click', function(e) {
			videoWindow1.fireEvent('focus');
		});
	}
});

//Function that listens for focus on video window
videoWindow2.addEventListener('focus',function(e)
{
	var network = Ti.Network;
	if(network.online)
	{
		if(!VARS.video2Initialized)
		{
			Video.createVideoGrid({
				user_yt:VARS._YT_USERNAME,
				limit:VARS._VIDEO_COUNT,
				playlist:'PL45D00A9F6BE69F91',
				win:videoWindow2,
			});
			VARS.video2Initialized=true;
		}
	}
	else if (!VARS.video2Initialized)
	{
		Ti.UI.createAlertDialog({
			title:'No Internet Connection',
			message:'Please connect to the Internet',
			buttonNames:['Close']
		}).show();
		
		var button2 = Ti.UI.createButton({
			title:'Refresh',
			width:100,
			height:30
		});
		 videoWindow2.add(button2);
		button2.addEventListener('click', function(e) {
			videoWindow2.fireEvent('focus');
		});
	}
});

videoWindow3.addEventListener('focus',function(e)
{
	var network = Ti.Network;
	if(network.online)
	{
		if(!VARS.video3Initialized)
		{
			Video.createVideoGrid({
				user_yt:VARS._YT_USERNAME,
				limit:VARS._VIDEO_COUNT,
				playlist:'PL0LPMtuobLNmSKNKjQkKoOqyDAHYiB4Mx',
				win:videoWindow3,
			});
			VARS.video3Initialized=true;
		}
	}
	else if (!VARS.video3Initialized)
	{
		Ti.UI.createAlertDialog({
			title:'No Internet Connection',
			message:'Please connect to the Internet',
			buttonNames:['Close']
		}).show();
		
		var button = Ti.UI.createButton({
			title:'Refresh',
			width:100,
			height:30
		});
		 videoWindow3.add(button);
		button.addEventListener('click', function(e) {
			video3Initialized=false;
			videoWindow3.fireEvent('focus');
		});
	}
});


//Function that listens for focus on contact window
videoSearchWindow.addEventListener('focus',function(e)
{
	indicator.closeIndicator();
});

videoWindow4.addEventListener('focus',function(e)
{
	var network = Ti.Network;
	if(network.online)
	{
		if(!VARS.video4Initialized)
		{
			Video.createVideoGrid({
				user_yt:VARS._YT_USERNAME,
				limit:10,
				playlist:'PLC74A14923ED95F5C',
				orderby:'viewCount',
				win:videoWindow4,
			});
			VARS.video4Initialized=true;
		}
	}
	else if (!VARS.video4Initialized)
	{
		Ti.UI.createAlertDialog({
			title:'No Internet Connection',
			message:'Please connect to the Internet',
			buttonNames:['Close']
		}).show();
		
		var button = Ti.UI.createButton({
			title:'Refresh',
			width:100,
			height:30
		});
		 videoWindow4.add(button);
		button.addEventListener('click', function(e) {
			video4Initialized=false;
			videoWindow4.fireEvent('focus');
		});
	}
});
//videoWindow4.fireEvent('focus');



videoWindow5.addEventListener('focus',function(e)
{
	
	var network = Ti.Network;
	if(network.online)
	{
		if(!VARS.video5Initialized)
		{
			Video.createVideoGrid({
				user_yt:VARS._YT_USERNAME,
				limit:20,
				orderby:'published',
				win:videoWindow5,
			});
			VARS.video5Initialized=true;
		}
	}
	else if (!VARS.video5Initialized)
	{
		Ti.UI.createAlertDialog({
			title:'No Internet Connection',
			message:'Please connect to the Internet',
			buttonNames:['Close']
		}).show();
		
		var button = Ti.UI.createButton({
			title:'Refresh',
			width:100,
			height:30
		});
		 videoWindow5.add(button);
		button.addEventListener('click', function(e) {
			video5Initialized=false;
			videoWindow5.fireEvent('focus');    
		});
	}
});
//Function that listens for focus on contact window
contactWindow.addEventListener('focus',function(e)
{
	
	
	if(!VARS.contactInitialized)
	{  
		createContact();
		VARS.contactInitialized=true;
	}
});

//Function that listens for focus on contact window
linkWindow.addEventListener('focus',function(e)
{ 
	if(!VARS.linksInitialized)
	{
		createLinks();
		VARS.linksInitialized=true;
	}
});

//Function that listens for focus on about window
aboutWindow.addEventListener('focus',function(e)
{
	if(!VARS.aboutInitialized)
	{
		createAboutWeb();
		VARS.aboutInitialized=true;
	}
});

soundWindow.addEventListener('focus',function(e)
{
	if(!VARS.soundInitialized)
	{
		createSound();
		VARS.soundInitialized = true;
	}
});



purchasesWin.addEventListener('focus', function(e)
{
	//include('common/purchases.js');
});



restorepurchasesWin.addEventListener('focus', function(e)
{
	var widhtlbl = "300dp";
	var fontsizelbl = "16dp";
	var leftlbl = "8dp";
	
	if(VARS._platform == VARS._iPad){
		widhtlbl = "900dp" ;
		fontsizelbl = "18dp";
		leftlbl = "16dp";
	}
	//Titanium.Platform.
	//Create Image descriptiom
	var restoreDesc=Ti.UI.createLabel({
		text:'If you have paid to remove ads, but have deleted and re-installed this app, visit this page and login with your Apple ID. This will restore your purchase and remove the ads in your app. Thanks for using the BoredShortsTV app!',
		color:VARS.formTextColor,
		top:"5dp",
		left:leftlbl,
		font:{fontSize:fontsizelbl},
		width:widhtlbl,
		height:"150dp"
	});
	restorepurchasesWin.add(restoreDesc);
	
	//include('common/purchases.js');
	var restP = require('common/removeAds');
	var isRestore = Ti.App.Properties.getBool('isRestore');
	
	if(!isRestore){
		restP.restorePurchases();
	}else{
		Ti.UI.createAlertDialog({title:L('Alert !'), message:L('You have not purchases to restore or have already restored.')}).show();	
	}
});

		