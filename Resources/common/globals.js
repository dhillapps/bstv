//################# STATIC DATA - DO NOT CHANGE #####################
exports._NEXT_GEN = "nextgen";
exports._FB_GALLERY = "fb";
exports._PICASA = "pic";
exports._FLICKR = "flic";
exports._FB = "fb";
exports._RSS = "rss";
exports._WP = "wp";
exports._TW = "tw";
exports._JOOMLA = "joomla";
exports._SINGLE_MAP = "sm";
exports._YT = "yt";
exports._VIMEO = "vimeo";

exports._iPad = "ipad";
exports._iPhone = "iphone";
exports._android = "android";

exports._platform = Ti.Platform.osname;
exports._iOS = (Ti.Platform.osname == "ipad" || Ti.Platform.osname == "iphone");

//################# USER DATA - CHANGES HERE ######################

//Indicator to see if script is initialized
exports.galleryInitialized = false;
exports.newsInitialized = false;
exports.videoInitialized = false;
exports.video1Initialized = false;
exports.video2Initialized = false;
exports.video3Initialized = false;
exports.video4Initialized = false;
exports.video5Initialized = false;
exports.contactInitialized = false;
exports.linksInitialized = false;
exports.aboutInitialized = false;
exports.soundInitialized = false;

//Navigation Settings
exports._NavigationType = "tab", // Possible:  tab, dash, list, metro

//CHANGE THE TYPE OF NEWS HERE
exports._typeOfNews = exports._TW;
//Possible exports._WP  , exports._RSS , exports._TW, exports._JOOMLA

//CHANGE THE TYPE OF GALLERY HERE
exports._typeOfGallery = exports._FB_GALLERY;
// Possible Values:  exports._FB_GALLERY, exports._NEXT_GEN, exports._PICASA, exports._FLICKR

//CHANGE THE TYPE OF VIDEOS HERE
exports._typeOfVideo = exports._YT;
// Possible Values: exports._YT, exports._VIMEO

//How long the connection to api should take
exports._timeout = 10000;

//Settings for facebook
//Ti.Facebook.appid = "134793934930"; //Change this with your own Facebook app id
//Ti.Facebook.permissions = ['publish_stream', 'read_stream'];
//Ti.Facebook.forceDialogAuth = true;

//News settings
exports._WordPress_URL = "http://nextwebart.net/demo/wordpress/";
exports._JOOMLA_URL = "http://www.nextwebart.net/demo/joomla/";
exports._RSS_URl = "http://v2.0.news.tmg.s3.amazonaws.com/feeds/news.xml";
exports._RSS_URl = "http://feeds.feedburner.com/MobileTuts?format=xml";
exports._MY_RSS_IS_PERMALINK = false;
//Change if you have problems with images in RSS
exports._MY_RSS_PERMALINK = "http://mobiletuts.net/";
//Used to fix the location to the images
exports._TWITTER_USER = "boredshortstv";
exports._count = 5;
//Limit of the articles / tweets / post returned
exports._WP_TIMTHUMB = "http://nextwebart.net/demo/wordpress/timthumb.php";
//Location to WordPress Timthumb script- OPTIONAL
exports._JOOMLA_TIMTHUMB = "http://www.nextwebart.net/demo/joomla/timthumb.php";
//Location to Joomla Timthumb script- OPTIONAL
exports._MY_TIMTHUMB_ALLOWS_EXTERNAL = false;
//If your timthumb instalations allows external images then set to true. ( used fro icon in the list )
exports._directLinkToTwitter = false;
exports._imageNewsDefault = "/appicon.png";
//Input local or remote path to image that you want to use as defaut image if there is no image for the specific article
exports._shareTitle = "Check out this article ";
exports._displayLoadMore = true;

//Gallery Settings
exports._NextGen_Address = "http://nextwebart.com/clients/lucy/wp-content/";
//This is the structure of the url. for more info read the docs
exports._PicasaUserId = "107364038100454040000";
//FIND your picasa user id and enter it here
exports._FlickrAppKEY = "1e41628a60f96bec1d00c9c6453f9ebf";
// REGISTER for your own Flick APP ID, This will be functional for short period of time
exports._FlickrUserID = "52617155@N08";//FIND YOUR FLICKR USER ID ---> http://idgettr.com/
exports._FBPageID = '453725481337561';
//Replace it with your own facebook page id or page name
exports._TIMTHUMB_FOR_GALLERY = "http://nextwebart.net/demo/timthumb.php";
//Location to Titmthum used for gallery ( can be same as WP_TIMTHUMB AND JOOMLA_TIMTHUMB ) -- THIS IS FOR DEMO PURPOSES ONLY> PLS  CHANGE WHEN YOU MAKE YOUR APP.
exports._TIMTHUMB_FACEBOOK_ENABLED = true;
// Set this to false, if you timthumb doesn't accept facebook ( Read documentation )
exports._IMAGE_QUALITY = "100";
//Set lower number if image loading is slow

exports._DisplayDescription = true;
//true / false  to display description in the image if it exists
exports._PhotoLimitFacebook = 30;
//Change the value here how many photos you want to fetch

//Video Settings
exports._YT_USERNAME = "BoredShortsTV";
exports._VIMEO_USERNAME = "brad";
exports._VIDEO_COUNT = 50;
exports._showVideoCoverShadow = true;

//Links settings
exports._LINKS = [
/* {
 name:"Google+",
 link:"http://www.googleplus.com"
 },*/
{
	name : "Facebook",
	link : "http://www.facebook.com/boredshortstv"
}, {
	name : "Official Web Site",
	link : "http://www.boredshorts.tv",
}, {
	name : "Twitter",
	link : "https://twitter.com/#!/boredshortstv",
}];

//Contact Data
exports.contactTitle = "Need help? Want An App?";
exports.contactImage = "/contact.png";
exports.contactImageTitle = "Dustin Hill";
exports.contactImageDesc = "Contact me with support questions, positive feedback, and feature requests. \n\nAlso, contact me if you're looking to make a mobile app. :)";
exports.contactNumber = "8013805532";
exports.contactEmail = "me@hiredustinhill.com";
exports.contactSite = "http://hiredustinhill.com";
//exports.contactInfoText="";
exports.contactPHPLocation = "http://nextwebart.com/clients/sendmail.php";

//The format that date us displayed in
exports._DateFormat = "yyyy-MMM-d HH:mm";

//About section
exports._About_URL = "http://www.boredshorts.tv";
//Replace this with your own link, like info about your app, or ink to your facebook / twitter portfolio

//PUSH Notification for iPhone
exports.useUrbanAirship = true;
exports.URBAN_AIRSHIP_APP_KEY = "MnGEE3jHSTerG3VS9XMdPA";
exports.URBAN_AIRSHIP_MASTER_SECRET = "esMqRWhNQLKauii0UYrU9Q";

//ADMOB ADS
exports.iphonePublisher_id = "a15178bc5574855";
//YOUR iPhone PUBLISHER ID HERE  - Leave empty if you don't want to display ads
exports.ipadPublisher_id = "a15178ba5ec6e1b";
//YOUR iPad PUBLISHER ID HERE
exports.androidPublisher_id = "a15178bda713b24";
if (exports._platform == exports._iPad && exports.ipadPublisher_id != "") {
	exports.displayAdMobAds = true;
}
if (exports._platform == exports._iPhone && exports.iphonePublisher_id != "") {
	exports.displayAdMobAds = true;
}

exports.displayAdMobAdsNavigation = (exports.displayAdMobAds && exports._iOS && true);
//Change from true to false to hide the ads in navigation
exports.displayAdMobAdsNews = (exports.displayAdMobAds && exports._iOS && true);
//Change from true to false to hide the ads in news
exports.displayAdMobAdsGallery = (exports.displayAdMobAds && exports._iOS && false);
//Change from true to false to hide the ads in gallery
exports.displayAdMobAdsVideos = (exports.displayAdMobAds && true);
//Change from true to false to hide the ads in videos
exports.displayAdMobAdsLink = (exports.displayAdMobAds && exports._iOS && true);
exports.displayAdMobAdsSound = (exports.displayAdMobAds && true);

var iphoneAdHeight = 50;
var iphoneAdWidth = 320;
var ipadAdHeight = 90;
var ipadAdWidth = 728;
exports.AdHeight = iphoneAdHeight;
exports.AdWidth = iphoneAdWidth;
if (exports._platform == exports._iPad) {
	exports.AdHeight = ipadAdHeight;
	exports.AdWidth = ipadAdWidth;
}

exports.AdHeightDisplayed = exports.AdHeight;
exports.AdWidthDisplayed = exports.AdWidth;
if (exports._NavigationType == "tab") {
	exports.AdHeightDisplayed += 50;
	//Because tabs has some width
}

var hideAds = require('common/removeAds');
var isPaid = hideAds.alreadyPaidToRemoveAds();

if (isPaid) {
	exports.displayAdMobAds = false;
	exports.displayAdMobAdsNavigation = false;
	exports.displayAdMobAdsNews = false;
	exports.displayAdMobAdsGallery = false;
	exports.displayAdMobAdsVideos = false;
	exports.displayAdMobAdsLink = false;
	exports.displayAdMobAdsSound = false;
}

//################# GLOBAL COLORS ###################
var Template = require('ui/templates/appstore');
// Change with one of the templates.
exports.windowBackgroundColor = 'white';
//Template.windowBackgroundColor;
exports.barColor = 'black';
//Template.barColor;
exports.separatoColor = Template.separatoColor;
exports.rowColor = Template.rowColor;
exports.rowOddColor = Template.rowOddColor;
exports.textColor = Template.textColor;
exports.textSecondColor = Template.textSecondColor;
exports.listColors = Template.listColors || ["#fe4819", "#d1005d", "#0081ab", "#009a3d"];

//Optional colors in the templates for videooo
exports.videosBackgroundColor = Template.videosWindowBackgroundColor || Template.windowBackgroundColor;
exports.videosBarColor = Template.videosCoversColor || Template.rowColor;
exports.videosTextColor = Template.videosTextColor || Template.textColor;

//Optional colors in th template for contact form
exports.fontColor = Template.fontColor || Template.textColor || "#ffffff";
exports.imageFrameColor = Template.imageFrameColor || Template.rowOddColor || "black";
exports.formTextColor = Template.formTextColor || Template.textColor || "white";
exports.formBgColor = '#a5a5a5' || "white";

//Optionsal colors for the reloader in news section
exports.reloadBackgroundColor = "#f0f0f0";
exports.reloadTextColor = "#444444";

exports.buttonColor = Template.buttonColor || "#010101";
exports.buttonTopColor = Template.buttonTopColor || "#010101";
exports.buttonBottomColor = Template.buttonBottomColor || "#333333";

//################# TEXT FORMATS ###################
exports.h1 = {
	fontSize : "18dp",
	fontStyle : 'bold',
	fontWeight : 'bold'
};
exports.h2 = {
	fontSize : "15dp",
	fontStyle : 'bold',
	fontWeight : 'bold'
};
exports.h3 = {
	fontSize : "12dp",
	fontStyle : 'bold',
	fontWeight : 'bold'
};
exports.normal = {
	fontSize : "11dp"
};
exports.empahasys = {
	fontSize : "10dp",
	fontStyle : 'italic'
};

//################ DISPLAY SETTINGS ################
exports._dpiWidth = 320;
//Standard for iPhone and most of android devices
exports._dpiHeight = 480;

//Standard for iPhone <=4s
//But is is not same in some andorid Devices that have XHDI like Samsung S3 and HTC ONE X
if (Ti.Platform.osname == "android") {
	exports._dpiWidth = (Ti.Platform.displayCaps.platformWidth / Ti.Platform.displayCaps.logicalDensityFactor);
}

if (exports._platform == exports._iPhone) {
	
	exports._dpiHeight = (Ti.Platform.displayCaps.platformHeight) - 65;
	alert("1 -- "+exports._dpiHeight+" ===== "+Ti.Platform.displayCaps.platformHeight);
	//Titanium.UI.iPhone.statusBarHidden = true;
	exports._dpiHeight = 568;
	//-65 for navigation
}

//Recalculate desity for iPad
if (Ti.Platform.osname == "ipad") {
	exports._dpiWidth = 768;
	exports._dpiHeight = (Ti.Platform.displayCaps.platformHeight) - 65;
	//-65 for navigation
}

function createWebViewButtons(webwindow, url) {
	var current_url = url;

	// Loading Indigator
	var loadIndBg = Ti.UI.createView({
		id : "loadIndBg",
		width : 70,
		height : 70,
		backgroundColor : '#000000',
		borderRadius : 8,
		visible : false,
		opacity : 0.6
	});

	// webwindow.add(loadIndBg);

	var loadInd = Ti.UI.createActivityIndicator({
		height : 50,
		width : 50,
		style : Ti.UI.iPhone.ActivityIndicatorStyle.BIG
	});
	//webwindow.add(loadInd);

	//create toolbar

	var toolBg = Ti.UI.createView({
		bottom : 8,
		width : 150,
		height : 40,
		backgroundColor : '#000000',
		borderRadius : 8,
		opacity : 0.6
	});
	webwindow.add(toolBg);

	// create a back button image
	var back_button = Ti.UI.createButton({
		backgroundImage : 're_l_prev_06.png',
		enabled : true,
		//bottom:20,
		left : 20,
		top : 12,
		width : 14,
		height : 16
		//   backgroundColor:'#000000'
	});
	toolBg.add(back_button);
	back_button.addEventListener('click', function() {
		webwindow.goBack();
	});

	// create a forward button image
	var forward_button = Ti.UI.createButton({
		backgroundImage : 're_l_next_06.png',
		enabled : false,
		//  bottom:20,
		top : 12,
		right : 20,
		width : 14,
		height : 16
		//   backgroundColor:'#000000'
	});
	toolBg.add(forward_button);
	forward_button.addEventListener('click', function() {
		webwindow.goForward();
	});
	var refresh_button = Ti.UI.createButton({
		backgroundImage : 're_l_03.png',
		enabled : false,
		//bottom:20,
		top : 12,
		width : 14,
		height : 16
		//   backgroundColor:'#000000'
	});
	toolBg.add(refresh_button);
	refresh_button.addEventListener('click', function() {
		webwindow.reload();
	});

	webwindow.addEventListener('beforeload', function(e) {
		// loadIndBg.opacity = 0.6;
		//loadInd.opacity = 1;
		loadIndBg.show();
		loadIndBg.visible = true;
	});
	webwindow.addEventListener('load', function(e) {

		loadIndBg.visible = false;

		//loadIndBg.opacity = 0;
		//loadInd.opacity = 0;
		// set the control buttons
		if (webwindow.canGoForward()) {
			forward_button.enabled = true;
		} else {
			forward_button.enabled = false;
		}
		if (webwindow.canGoBack()) {
			back_button.enabled = true;
		} else {
			back_button.enabled = false;
		}
		current_url = e.url;
	});

}

exports.createWebViewButtons = createWebViewButtons;
