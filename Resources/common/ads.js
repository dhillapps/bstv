var VARS=require('common/globals');
var removeAds = require('common/removeAds');
var Admob = require('ti.admob');
exports.requestAd=function(p)
{

	var bottom = 0;
	if(p.bottom)
		bottom = p.bottom;
	
	if(VARS._platform==VARS._iPhone)
	{
		var adMobView = Admob.createView({
   			 publisherId:VARS.iphonePublisher_id,
   			 width:VARS.AdWidth,
   			 bottom:0,
   			 height:VARS.AdHeight,    
		});
		var adMobWin = Ti.UI.createWindow({
			width:VARS.AdWidth,
			bottom:bottom,
			height:VARS.AdHeight,
			border:0
		});
		var removeAdButton = Ti.UI.createButton({
			height:22,
			width:22,
			bottom:VARS.AdHeight-22,
			right:0,
			opacity:0.7,
			backgroundImage:"removeAdsiPhone.png"
		});
		removeAdButton.addEventListener('click', function(e) {
			//alert('Would you like to remove ads for $1.99?');
			var success = removeAds.removeAds();
			
			if(success)
			{
				p.win.remove(adMobWin);
				p.win.fireEvent('focus');
			}
		});
		adMobWin.add(adMobView);
		adMobWin.add(removeAdButton);
		p.win.add(adMobWin);
	}
	else if(VARS._platform==VARS._iPad)
	{
		var adMobView = Admob.createView({
   			 publisherId:VARS.ipadPublisher_id,
   			 width:VARS.AdWidth,
   			 bottom:0,
   			 height:VARS.AdHeight   
		});
		var adMobWin = Ti.UI.createWindow({
			width:VARS.AdWidth,
			bottom:bottom,
			height:VARS.AdHeight,
			border:0
		});
		var removeAdButton = Ti.UI.createButton({
			height:35,
			width:35,
			bottom:VARS.AdHeight-35,
			right:0,
			opacity:0.7,
			backgroundImage:"removeAdsiPad.png"
		});
		removeAdButton.addEventListener('click', function(e) {
			//alert('Would you like to remove ads for $1.99?');
			var success = removeAds.removeAds();
			
			if(success)
			{
				p.win.remove(adMobWin);
				p.win.fireEvent('focus');
			}
		});
		adMobWin.add(adMobView);
		adMobWin.add(removeAdButton);
		p.win.add(adMobWin);
	}
	else if(VARS._platform==VARS._android)
	{
		var adMobView = Admob.createView({
   			 publisherId:VARS.androidPublisher_id,
   			 width:VARS.AdWidth,
   			 bottom:0,
   			 height:VARS.AdHeight
		});
		var adMobWin = Ti.UI.createWindow({
			width:VARS.AdWidth,
			bottom:bottom,
			height:VARS.AdHeight,
			border:0
		});
		var removeAdButton = Ti.UI.createButton({
			height:27,
			width:27,
			bottom:VARS.AdHeight-27,
			right:0,
			opacity:0.7,
			backgroundImage:"removeAdsiPhone.png"
		});
		removeAdButton.addEventListener('click', function(e) {
			alert('Would you like to remove ads for $1.99?');
			removeAds.removeAds();
			p.win.remove(adMobWin);
			p.win.fireEvent('focus');

		});
		adMobWin.add(adMobView);
		adMobWin.add(removeAdButton);
		p.win.add(adMobWin);
	}
};
