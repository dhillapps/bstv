/**
* Indicator window with a spinner and a label
* Found on: http://glatter-gotz.com/blog/2012/03/19/appcelerator-titanium-a-nice-looking-activity-indicator-window/
* @param {Object} args
*/
function createIndicatorWindow(args) {
	
    var args = args || {};
    var loadingText = args.text || "Loading Videos..."
    var width = args.width || 200;
    var height = 50;
    
    var win = Titanium.UI.createWindow({
        height:height,
        width:width,
        borderRadius:10,
        touchEnabled:false,
        backgroundColor:'#000',
        opacity:0.8
    });
    
    if(args.top)
    	win.top = args.top;
    
    var view = Ti.UI.createView({
        width:Ti.UI.SIZE,
        height:Ti.UI.FILL,
        center: {x:(width/2), y:(height/2)},
        layout:'horizontal'
    });
    
    function osIndicatorStyle() {
        style = Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN;
        
        if ('iPhone OS' !== Ti.Platform.name) {
            style = Ti.UI.ActivityIndicatorStyle.DARK;
        }
        
        return style;
    }
     
    var activityIndicator = Ti.UI.createActivityIndicator({
        style:osIndicatorStyle(),
        left:0,
        height:Ti.UI.FILL,
        width:30
    });
    
    var label = Titanium.UI.createLabel({
        left:10,
        width:Ti.UI.FILL,
        height:Ti.UI.FILL,
        text:loadingText,
        color: '#fff',
        font: {fontFamily:'Helvetica Neue', fontSize:16, fontWeight:'bold'},
    });

    view.add(activityIndicator);
    view.add(label);
    win.add(view);

    function openIndicator() {
      
        win.open();
        activityIndicator.show();
    }
    
    win.openIndicator = openIndicator;
    
    function closeIndicator() {
    	
       activityIndicator.hide();
        win.close();
    }
    
    win.closeIndicator = closeIndicator;
    
    return win;
}

// Public interface
exports.createIndicatorWindow = createIndicatorWindow;