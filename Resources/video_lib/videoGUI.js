
var VARS =require('common/globals'); //Here we store all global variables / settings

/**
 * Creates Videos Tiles
 * @param {Object} p
 * @config {Objrct} video, the video to be shown
 * @config {Number} width, the width on the tile
 * @config {Number} height, the height of the tile
 * @config {Number} top, the top on the tile
 * @config {Number} left, the left of the tile
 * @config {View} tab, tab to open the video in
 * 
 */
exports.createVideoTile=function(p)
{
	
	//Create the base
	var theBase=Ti.UI.createView({
		video:p.video,
		top:p.top,
		left:p.left,
		width:p.width,
		height:p.height,
		backgroundColor:VARS.videosBackgroundColor,
	});
	//The video image
	var theVideoImage=Titanium.UI.createImageView({
		image:p.video.icon,
		top:"0dp",
		left:"0dp",
		width:p.width,
		backgroundColor:VARS.videosTextColor,
		height:p.height,
		preventDefaultImage:true,
		touchEnabled:false,
	});
	theBase.add(theVideoImage);
	
	
	//The cover view
	var shadowTop="33dp"
	if(VARS._platform==VARS._android){shadowTop="7dp"}
	var theImageShadow=Ti.UI.createImageView({
		image:"/videoCover.png",
		top:shadowTop,
		opacity:0.5,
		touchEnabled:false,
		width:p.width,
		height:p.height,
	})
	if(VARS._showVideoCoverShadow)
	{
		theBase.add(theImageShadow);
	}
	
	var theTopBar=Ti.UI.createView({
		backgroundColor:VARS.videosBarColor,
		top:"0dp",
		height:"33dp",
		touchEnabled:false,
	})
	var userTitleLeft="5dp";
	if(p.video.avatar)
	{
		userTitleLeft="25dp";
		
		//Create user image
		var userAvatar=Ti.UI.createImageView({
			image:p.video.avatar,
			left:"5dp",
			height:"15dp",
			width:"15dp",
			touchEnabled:false,
		})
		theTopBar.add(userAvatar);
	}
	var userTitle=Ti.UI.createLabel({
		text:p.video.author,
		left:userTitleLeft,
		color:VARS.videosTextColor,
		font:{fontSize:"12dp"},
		touchEnabled:false,
		
	})
	theTopBar.add(userTitle);
	
	//Create the time holder
	var timeHolder=Ti.UI.createView({
		width:"40dp",
		height:"15dp",
		right:"5dp",
		borderRadius:"3dp",
		backgroundColor:VARS.videosTextColor,
		touchEnabled:false,
	})
	var minutes = Math.floor(p.video.duration/60);
	var seconds = (p.video.duration-minutes*60);
	if(seconds < 10)
		seconds = "0"+seconds;
	var durationText=Ti.UI.createLabel({
		text:minutes+":"+seconds,
		color:VARS.videosBarColor,
		font:{fontSize:"12dp"},
		touchEnabled:false,
	})
	timeHolder.add(durationText);
	theTopBar.add(timeHolder);
	theBase.add(theTopBar);
	
	var theBottomBar=Ti.UI.createView({
		backgroundColor:VARS.videosBarColor,
		bottom:"0dp",
		height:"50dp",
		touchEnabled:false,
	})
	var theTitle=Ti.UI.createLabel({
		text:p.video.title,
		font:VARS.h2,
		color:VARS.videosTextColor,
		top:"3dp",
		left:"5dp",
		width:"310dp",
		touchEnabled:false,
		height:"20dp"
	})
	var theViews=Ti.UI.createLabel({
		text:p.video.views,
		font:{fontSize:"12dp"},
		color:VARS.videosTextColor,
		textAlign:"right",
		bottom:"3dp",
		left:"5dp",
		width:"310dp",
		touchEnabled:false,
		height:"18dp"
	})
	theBottomBar.add(theTitle);
	theBottomBar.add(theViews);
	
	
	//Make it beautiful
	var shadowBar=Ti.UI.createView({
		width:p.width,
		height:"1dp",
		backgroundColor:VARS.videosTextColor,
		bottom:0,
		touchEnabled:false,
	})
	theBottomBar.add(shadowBar);
	
	theBase.add(theBottomBar);
	
	
	
	
	return theBase;
}