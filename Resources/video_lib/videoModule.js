/*
* @version 1.1
*/

//Include required modules
var VARS =require('common/globals'); //Here we store all global variables / settings
var API_video=require('video_lib/videoAPI');
var GUI_video=require('video_lib/videoGUI');
if(VARS.displayAdMobAdsVideos){var ADS=require('common/ads');} //AD LIBRARY
/**
 * Creates video Grid
 * @param {Object} p
 * @config {String} user_yt, the YT username
 * @config {String} user_vimeo, the VIMEO username
 * @config {Number} limit, the number of results
 * @config {View} win, the window that we are going to assign data to it
 * @config {Tab} tab, the tab that this videoModule is oppened in
 */
exports.createVideoGrid=function(p)
{
	
	//############### VIEWS ################

	var theVideoWindow=p.win;
    var webviewyt  ;
    var videoScroller;
    
    //videoScroller.removeAllChildren();
	theVideoWindow.backgroundColor = "#000" ;
	// Refresh button
	var refreshButton = Ti.UI.createButton({
		systemButton: Ti.UI.iPhone.SystemButton.REFRESH
	});
	
	refreshButton.addEventListener('click', function(e) {
		exports.createVideoGrid({
			user_yt:VARS._YT_USERNAME,
			limit:VARS._VIDEO_COUNT,
			playlist:p.playlist,
			win:theVideoWindow
		});
	});
	
	
	var searchBar;

	if(!p.query)
	{
		theVideoWindow.rightNavButton = refreshButton;
	}
	else
	{
		searchBar = p.searchbar;
	}
	
	
	//Retreived videos
	var videos=null;
	
	var load = require('video_lib/load');
	var indicator = load.createIndicatorWindow();

	//################### FUNCTIONS ####################
	createYoutubeStyleList=function(data)
	{
		
		indicator.openIndicator();
		//Static w/h of the tiles
		var tileWidth="320dp";
		var tileHeight="240";
		if(VARS._platform==VARS._android)
			{
				tileWidth=VARS._dpiWidth+"dp";
				tileHeight=240*(VARS._dpiWidth/320);
			}
		
		
		//AD
		var videoScrollerHeight="100%";
		
		if(VARS.displayAdMobAdsVideos)
		{
			var theAd=ADS.requestAd({win:theVideoWindow});
			videoScrollerHeight=VARS._dpiHeight-VARS.AdHeightDisplayed;
		}	
		var topBar = 0;
		if(p.query)
		{
			videoScrollerHeight=VARS._dpiHeight - p.searchbar.height;
			topBar = p.searchbar.height;
		}
		
		//Crete the scroller
		 videoScroller=Ti.UI.createScrollView({
			width:"100%",
			height:videoScrollerHeight,
			contentWidth:"auto",
			contentHeight:"auto",
			top:'topBar',
			left:0,
			backgroundColor:'#000',
		});
		try
		{	
		videoScroller.removeAllChildren();
		}
		catch (e)
		{
			
		}
		
		var row=0;
		for(var i=0;i<data.length;i++)
		{
			var topVI=(tileHeight*i+10*i+0);
			if(p.query)
				topVi=topVI+p.searchbar.height;
			var leftVI=0;
			if(VARS._platform==VARS._iPad)
			{
				topVI=(tileHeight*row+15*row);
				if((i%2)!=0){leftVI=320+238;row++;}
				else {leftVI=148;}
			}
			
			var videoItem=GUI_video.createVideoTile({
				video:data[i],
				left:leftVI+"dp",
				top:topVI+"dp",
				width:tileWidth,
				height:tileHeight+"dp",
				tab:p.tab,
			});
			videoItem.addEventListener('click',function(e)
			{			
					videoWindow=Ti.UI.createWindow({
	    				title:e.source.video.title,
						backgroundColor :'#000',
						navBarHidden : false,
						barColor : VARS.barColor,						
					});
					
					var height = VARS._dpiHeight*0.6;
					//alert("win -- "+height);
					var htmlheader = "<html><head></head><body style='margin:0; background:#000'><iframe id='yt' src='http://www.youtube.com/embed/";
					var htmlfooter = "?rel=0' type='application/x-shockwave-flash' width='99%' height='"+height+"'></iframe>";
					var videoURL = e.source.video.link;
					var videoID = videoURL.split('v=')[1];
					var ampersandPosition = videoID.indexOf('&');
					if(ampersandPosition != -1) {
  						videoID = videoID.substring(0, ampersandPosition);
					}
					
					if(videoScrollerHeight != '100%')
						videoScrollerHeight = videoScrollerHeight + 10;
					
					var description = "<div><p style='padding: 0px 0px 0 5px; width:90%; background:#000; color:#fff;'>"+e.source.video.desc+"</p></div></body></html>";
					var htmlmash = htmlheader + videoID + htmlfooter + description;
				   webviewyt = Titanium.UI.createWebView({
						html : htmlmash,
    						width: '100%',
    						height:videoScrollerHeight,
    						top:0,
    						left:0,
    						showScrollBars:false
					});
					
				/* Add the share bar Make to
					var shareButton = Ti.UI.createButton({
						title:'share',
						systemButton:Ti.UI.iPhone.SystemButton.ACTION
					});
					
					var toolbar = Ti.UI.iOS.createToolbar({
						items:[shareButton],
						bottom:0,
						borderTop:true,
						borderBottom:false,
						barColor:'#000'
					});
					
					
					videoWindow.hideTabBar(); //Set nav bar to false above!!!!
				videoWindow.add(toolbar);
							*/
			
					videoWindow.add(webviewyt);
					
					
					theVideoWindow.containingTab.open(videoWindow);
					
					
					if(VARS.displayAdMobAdsVideos)
					{
						var theAd=ADS.requestAd({win:videoWindow,bottom:0});
						videoScrollerHeight=VARS._dpiHeight-VARS.AdHeightDisplayed;
					}
					

			
			});
		
			videoScroller.add(videoItem);
		
		}
		theVideoWindow.add(videoScroller);
			alert("win -- "+theVideoWindow.size.height);
		indicator.closeIndicator();
	};
	
	if(p.user_yt)
	{
		//Fetch YouTube Videos
		API_video.fetchYouTubeVideos({
			user:p.user_yt,
			limit:p.limit,
			query:p.query,
			playlist:p.playlist,
			orderby:p.orderby,
			listener:createYoutubeStyleList,
		});
	}
	else if(p.user_vimeo)
	{
		//Fetch Vimoe Videos
		API_video.fetchVimeoVideos({
			user:p.user_vimeo,
			limit:p.limit,
			listener:createYoutubeStyleList,
		});
	}
	//indicator.openIndicator();
};
