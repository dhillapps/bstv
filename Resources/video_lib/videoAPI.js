/**
 * @description
 *  Fetch videos from YouTube
 * @param {Object} param, Default Null
 * @config {Function} listener Listener for the result
 * @config {String} user
 * @config {Number} limit
 * @return ArrayList of videos
 */
exports.fetchYouTubeVideos = function(param) {
	

	//List of videos
	var videos = [];

	//
	// XHR GET
	//
		var load = require('video_lib/load');
	var indicator = load.createIndicatorWindow();
		indicator.openIndicator();
	var xhr = Titanium.Network.createHTTPClient();
	xhr.onload = function() 
	{
		 
		var jsonObject = JSON.parse(this.responseText);

		//if(jsonObject.feed.openSearch$totalResults.$t > 0)
		try 
		{
		jsonObject=jsonObject.feed.entry;
		
		for(var i = 0; i < jsonObject.length; i++) 
		{
			one_video = jsonObject[i];
            var viewCount = '';
            try{
            	viewCount = one_video.yt$statistics.viewCount+' '+'views';
            }
            catch (e) {
            	
            }
            videos.push({
            	title:one_video.title.$t,
            	desc:one_video.content.$t,
            	icon:one_video.media$group.media$thumbnail[0].url,
            	id:one_video.media$group.media$player[0].url,
            	views:viewCount,
            	author:one_video.author[0].name.$t,
            	duration:one_video.media$group.yt$duration.seconds,
            	date:one_video.published.$t,
            	link:one_video.media$group.media$player[0].url,
            });
            if(i==0)
            {
            	Ti.API.info(one_video);
            }
		}

		param.listener(videos);
		indicator.closeIndicator();
		}
		catch (e)
		{
			indicator.closeIndicator();
			Ti.UI.createAlertDialog({
			title:'No Results',
			message:'There were no results found on YouTube. Please try again :)',
			buttonNames:['Close']
		}).show();
		
		}
		
	};
	xhr.onerror = function() 
	{
		Ti.UI.createAlertDialog({
			title:'YouTube Error',
			message:'There was an error connecting to YouTube. Please try again',
			buttonNames:['Close']
		}).show();
		indicator.closeIndicator();
	};
	if(!(param.playlist)&&!(param.query))
	{
		var YTLink="http://gdata.youtube.com/feeds/api/users/"+param.user+"/uploads?alt=json&max-results="+param.limit;
	}
	else if(param.playlist)
	{
		var YTLink="http://gdata.youtube.com/feeds/api/playlists/"+param.playlist+"?alt=json&max-results="+param.limit;
	}
	else if(param.query)
	{
		var YTLink="http://gdata.youtube.com/feeds/api/users/"+param.user+"/uploads?q="+param.query+"&alt=json&max-results="+param.limit;
	}
	else if(param.orderby)
	{
		var YTLink="http://gdata.youtube.com/feeds/api/users/"+param.user+"/uploads?alt=json&orderby="+param.orderby+"&max-results="+param.limit;
	}
	Ti.API.info("YT link:"+YTLink);
	xhr.open("GET", YTLink);
	xhr.send({});
	
}


/**
 * @description
 *  Fetch videos from Vimeo
 * @param {Object} param, Default Null
 * @config {Function} listener Listener for the result
 * @config {String} user
 * @config {Number} limit
 * @return ArrayList of videos
 */
exports.fetchVimeoVideos = function(param) {
	//List of videos
	var videos = [];

	//
	// XHR GET
	//
	var xhr = Titanium.Network.createHTTPClient();
	xhr.onload = function() 
	{
		//alert(this.responseText);
		var jsonObject = JSON.parse(this.responseText);
		for(var i = 0; i < jsonObject.length; i++) 
		{
			one_video = jsonObject[i];
            videos.push({
            	title:one_video.title,
            	desc:one_video.description,
            	icon:one_video.thumbnail_large,
            	id:one_video.id,
            	views:one_video.stats_number_of_plays+' '+L('views'),
            	author:one_video.user_name,
            	duration:one_video.duration,
            	date:one_video.upload_date,
            	avatar:one_video.user_portrait_small,
            	link:one_video.mobile_url,
            });
            if(i==0)
            {
            	Ti.API.info(one_video);
            }
		}
        //Ti.API.info(videos);
		param.listener(videos);

	};
	xhr.onerror = function() 
	{
		Ti.UI.createAlertDialog({
			title:'YouTube Error',
			message:'There was an error connecting to YouTube. Please try again',
			buttonNames:['Close']
		}).show();
		indicator.closeIndicator();
	};
	var VIMOELink="http://vimeo.com/api/v2/"+param.user+"/videos.json";
	Ti.API.info("VIMEO link:"+VIMOELink);
	xhr.open("GET", VIMOELink);
	xhr.send({});
	indicator.closeIndicator();

}