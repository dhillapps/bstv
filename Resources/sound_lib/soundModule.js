var VARS =require('common/globals');
if(VARS.displayAdMobAdsSound){var ADS=require('common/ads');} //AD LIBRARY
var soundBoardWindow;
exports.createSoundBoard=function(p)
{
	 soundBoardWindow = p.win;
	
	// Create Search Bar
	var searchbar = Ti.UI.createSearchBar({
		barColor:"#000000",
		showCancel:false
	});
	
	// Create an empty table view
	var tableview = Ti.UI.createTableView({
		search:searchbar,
		hideSearchOnSelection: true,
		top:0,
		left:0
	});
	
	if(VARS.displayAdMobAdsSound)
	{
		var theAd=ADS.requestAd({win:soundBoardWindow});
		tableview.height=VARS._dpiHeight-VARS.AdHeightDisplayed;
	}

	
	// Create an array of data to pass into the tableview
	var soundData = [];
	var text = 'Get';
	var height = 30;
	var right = 10;
	var width = 50;
	var rowHeight = 45;
	
	
	// Object constructor
	function soundObj(title, path, url)
	{
		this.title = title;
		this.path = path;
		this.url = url;
	}
	
	// Array to store sound objects
	var sounds =[]
	
	// Create sound objects
	var sound = new soundObj('A Ho Dee Ho!', "files/ahodeeho.mp3","http://hiredustinhill.com")
	sounds.push(sound);
	var sound1 = new soundObj('Are Those My Friends?', "files/arethosemyfriends.mp3","http://hiredustinhill.com")
	sounds.push(sound1);
	var sound2 = new soundObj('Big Trak', "files/bigtrak.mp3","http://hiredustinhill.com")
	sounds.push(sound2);
	var sound3 = new soundObj('Big Trak Scream', "files/bigtrakscream.mp3","http://hiredustinhill.com")
	sounds.push(sound3);
	var sound4 = new soundObj('Bike To Face', "files/biketoface.mp3","http://hiredustinhill.com")
	sounds.push(sound4);
	var sound5 = new soundObj('Blow My Nose', "files/blowmynose.mp3","http://hiredustinhill.com")
	sounds.push(sound5);
	var sound6 = new soundObj('Boys Are Mermaids', "files/boysaremermaidstoo.mp3","http://hiredustinhill.com")
	sounds.push(sound6);
	var sound7 = new soundObj('Breath War', "files/breathwar.mp3","http://hiredustinhill.com")
	sounds.push(sound7);
	var sound8 = new soundObj('Buffer', "files/buffer.mp3","http://hiredustinhill.com")
	sounds.push(sound8);
	var sound9 = new soundObj('Completos', "files/completos.mp3","http://hiredustinhill.com")
	sounds.push(sound9);
	var sound10 = new soundObj("Doesn't Give Me Peas", "files/doesntgivemepeas.mp3","http://hiredustinhill.com")
	sounds.push(sound10);
	var sound11 = new soundObj("Don't Punch Our Car", "files/dontpunch.mp3","http://hiredustinhill.com")
	sounds.push(sound11);
	var sound12 = new soundObj('Facial Disgracial', "files/facialdisgracial.mp3","http://hiredustinhill.com")
	sounds.push(sound12);
	var sound13 = new soundObj('Fact', "files/fact.mp3","http://hiredustinhill.com")
	sounds.push(sound13);
	var sound14 = new soundObj('Hola', "files/hola.mp3","http://hiredustinhill.com")
	sounds.push(sound14);
	var sound15 = new soundObj("I Love You Mommy", "files/iloveyoumommy.mp3","http://hiredustinhill.com")
	sounds.push(sound15);
	var sound16 = new soundObj('Joy & Wonder', "files/joyandwonder.mp3","http://hiredustinhill.com")
	sounds.push(sound16);
	var sound17 = new soundObj('Jumping', "files/jumpingintrees.mp3","http://hiredustinhill.com")
	sounds.push(sound17);
	var sound18 = new soundObj('Know A Google', "files/knowagoogle.mp3","http://hiredustinhill.com")
	sounds.push(sound18);
	var sound19 = new soundObj('Lot Of Trouble', "files/lotoftrouble.mp3","http://hiredustinhill.com")
	sounds.push(sound19);
	var sound20 = new soundObj('Ninja Vanish', "files/ninjavanish.mp3","http://hiredustinhill.com")
	sounds.push(sound20);
	var sound21 = new soundObj('Ninja Vanish 2', "files/ninjavanish2.mp3","http://hiredustinhill.com")
	sounds.push(sound21);
	var sound22 = new soundObj('Out There You Weirdos', "files/outthereyouweirdos.mp3","http://hiredustinhill.com")
	sounds.push(sound22);
	var sound23 = new soundObj('Perfectly Normal Pancakes', "files/perfectlynormalpancakes.mp3","http://hiredustinhill.com")
	sounds.push(sound23);
	var sound24 = new soundObj('Poke Poke Poke', "files/pokepokepoke.mp3","http://hiredustinhill.com")
	sounds.push(sound24);
	var sound25 = new soundObj('Polly Pocket', "files/pollypocket.mp3","http://hiredustinhill.com")
	sounds.push(sound25);
	var sound26 = new soundObj("Punch You", "files/punchyou.mp3","http://hiredustinhill.com")
	sounds.push(sound26);
	var sound27 = new soundObj('Randay', "files/randay.mp3","http://hiredustinhill.com")
	sounds.push(sound27);
	var sound28 = new soundObj('Sorry I Just Go', "files/sorryijustgo.mp3","http://hiredustinhill.com")
	sounds.push(sound28);
	var sound29 = new soundObj('Stirrup', "files/stirruponpancakes.mp3","http://hiredustinhill.com")
	sounds.push(sound29);
	var sound30 = new soundObj('Struggling', "files/struggling.mp3","http://hiredustinhill.com")
	sounds.push(sound30);
	var sound31 = new soundObj('Super Delicious', "files/superdelicious.mp3","http://hiredustinhill.com")
	sounds.push(sound31);
	var sound32 = new soundObj('Sweet Tooth', "files/sweettooth.mp3","http://hiredustinhill.com")
	sounds.push(sound32);
	var sound33 = new soundObj('Tell You Again', "files/tellyouagain.mp3","http://hiredustinhill.com")
	sounds.push(sound33);
	var sound34 = new soundObj('Tofu Song', "files/tofusong.mp3","http://hiredustinhill.com")
	sounds.push(sound34);
	var sound35 = new soundObj('Toilet Papering', "files/toiletpapering.mp3","http://hiredustinhill.com")
	sounds.push(sound35);
	var sound36 = new soundObj("Track You Down", "files/trackyoudown.mp3","http://hiredustinhill.com")
	sounds.push(sound36);
	var sound37 = new soundObj('Weirdos', "files/weirdos.mp3","http://hiredustinhill.com")
	sounds.push(sound37);
	var sound38 = new soundObj('What In The Freak', "files/whatinthefreakjusthappened.mp3","http://hiredustinhill.com")
	sounds.push(sound38);

	
	for(var i = 0; i<sounds.length; i++)
	{
		var soundObj = sounds[i];
		
		var snd = Ti.UI.createTableViewRow({
			title:soundObj.title,
			path:soundObj.path,
			height:rowHeight
		});
		var btn = Ti.UI.createButton({
			right:right,
			height:height,
			width:width,
			title:text,
			color:'#ffffff',
			backgroundColor:'#000000',
			backgroundImage:'none',
			borderRadius:5
		});
		btn.addEventListener('click', function(e)
		{
		
			Ti.Platform.openURL(soundObj.url);
		})
		//snd.add(btn);
		soundData.push(snd);
	}

	tableview.setData(soundData);
	
	
	var sound = null;
	if(VARS._platform == VARS._android)
	{
		sound = Ti.Media.createSound(Ti.Filesystem.resourcesDirectory +"sound_lib/Fact.mp3");
	}
	else
	{
		sound = Ti.Media.createSound("sound_lib/Fact.mp3");

	}
	//sound.volume=0.0;
	sound.play();
	var filepath = null;
	tableview.addEventListener('click', function(e)
	{

		var i = e.index;
		
		if(VARS._platform == VARS._android)
		{
			filepath = Ti.Filesystem.resourcesDirectory + "sound_lib/" + soundData[i].path;
		}
		else
		{
			filepath = "sound_lib/"+ soundData[i].path;
		}
		
		if(sound != null)
		{
			if(!sound.isPlaying())
			{
				sound = Ti.Media.createSound({url:filepath});
				sound.play();
			}
			else
			{
				sound.stop();
				sound = Ti.Media.createSound({url:filepath});
				sound.play();
			}
		}
		else {
			sound = Ti.Media.createSound({url:filepath});
			sound.play();
		}	
	})
	
	soundBoardWindow.add(tableview);
	
	// stop sound when shift window to another tab
	soundBoardWindow.addEventListener('blur', function(e) {
		sound.stop();
	})

}


