/*
* @author DANIEL DIMOV
* @email dimovdaniel@yahoo.com
* @content news Module
* @version 1.0
*/
var VARS =require('common/globals');
var GUI_news=require('news_lib/newsGUI');
var API_news=require('news_lib/newsAPI');
if(VARS.displayAdMobAdsNews){var ADS=require('common/ads');} //AD LIBRARY
var activeArticle=null;

/**
 * @desc Creates list of news
 * @param {Object} p
 * @config {Number} type, type of News source to use [_FB,_RSS,_WP,_JOOMLA,_TW]
 * @config {View} win, the window that news should be displayed in
 * @config {View} tab, the tab to display the window in
 * ----------------_TW-------------------
 * @config {String} user, the tweeter username
 * ----------------_WP-------------------
 * @config {String} url, url of the WordPress blog
 * @config {number} count, limit results
 * @config {Number} cat, optional, only when filtering by category
 * ----------------_JOOMLA-------------------
 * @config {String} url, url of the Joomla blog
 * @config {number} count, limit results
 * @config {Number} cat, optional, only when filtering by category
 * ----------------_RSS-------------------
 * @config {String} url, url of the RSS Feed
 */
exports.createNewsList=function(p)
{
	
	
	//####################### DATA ###################
	var posts=[]; //List of posts/articles/tweets/items
	var type=p.type;
	var url=p.url;
	var page=1; //Current page
	
	var pulling = false;
	var reloading = false;
	var offset = 0;
	var loading=true;
	
	//###################### VIEWS ###################
	//AD
	newsListHeight="100%";
	if(VARS.displayAdMobAdsNews)
	{
		var theAd=ADS.requestAd({win:p.win});
		newsListHeight=VARS._dpiHeight-VARS.AdHeightDisplayed;
	}		
	var newsList=Ti.UI.createTableView({
		top:"0dp",
		width:"100%",
		height:newsListHeight,
		backgroundColor : VARS.windowBackgroundColor,
		separatorColor : VARS.separatoColor,
	})
	
	if(VARS._platform==VARS._android)
	{
		var androidActInd = Titanium.UI.createActivityIndicator({
			bottom : "10dp",
			width : "60dp",
			height : "60dp",
		});
	}
	
	
	
	
	if (VARS._iOS) {
		var categorisButton = Ti.UI.createButton({
			style : Ti.UI.iPhone.SystemButtonStyle,
			title : L('categories')
		})

		var optionsButton = Ti.UI.createButton({
			style : Ti.UI.iPhone.SystemButtonStyle,
			title : L('options')
		})

		var refreshButton = Ti.UI.createButton({
			systemButton : Titanium.UI.iPhone.SystemButton.REFRESH
		})

		var indicatorButton = Ti.UI.createButton({
			systemButton : Titanium.UI.iPhone.SystemButton.SPINNER
		})

		p.win.rightNavButton = indicatorButton;
	}

	
	//In iOS create the reloader header
	if(VARS._iOS)
	{
		var tableHeader=Ti.UI.createView({
			backgroundColor:VARS.reloadBackgroundColor,
			width:VARS._dpiWidth,
			height:60
		});
		
		var border=Ti.UI.createView({
			backgroundColor:VARS.reloadTextColor,
			bottom:0,
			height:2
		})
		tableHeader.add(border);
		
		
		var imageArrow=Ti.UI.createImageView({
			image:"pull.png",
			left:10,
			bottom:10,
			height:60,
		})
		tableHeader.add(imageArrow);
			
		var labelStatus=Ti.UI.createLabel({
			text:L('pulltoreload'),
			left:65,
			bottom:10,
			height:60,
			font:VARS.h2,
			color:VARS.reloadTextColor
		})
		tableHeader.add(labelStatus);
		
		var actInd=Ti.UI.createActivityIndicator({
			left:15,
			style:Titanium.UI.iPhone.ActivityIndicatorStyle.DARK,
			bottom:24,
			width:30,
			height:30,
		})
		tableHeader.add(actInd);
		
		//Assign the header
		newsList.headerPullView=tableHeader;
		
		
		
		
	}
	
	//####################### FUNCTIONS ##############
	function setPosts(thePosts)
	{
		loading=false;
		if(!p.cat&&(p.type == VARS._WP||p.type == VARS._JOOMLA)&&VARS._iOS)
		{
			p.win.rightNavButton=categorisButton;
		}
		else if(VARS._iOS)
		{
			p.win.rightNavButton=null;
		}
		//Remove the load more
		if(page>1&&(p.type == VARS._WP||p.type == VARS._JOOMLA)&&VARS._displayLoadMore)
		{
			posts.pop();
		}
		else if(page==1)
		{
			//Thsi is after pull to refresh, or in first call to api
			posts=[];
		}
		for(var i=0;i<thePosts.length;i++)
		{
			posts.push(GUI_news.creteSimpleRow(thePosts[i]));
		}
		newsList.setData(posts);
		
		
		//Add load more button in joomla and WordPress
		if(thePosts.length==VARS._count&&(p.type == VARS._WP||p.type == VARS._JOOMLA)&&VARS._displayLoadMore)
		{
			posts.push(GUI_news.createLoadMoreButton({size:posts.length}));
		}
		
		newsList.setData(posts);
		if(VARS._iOS)
		{
			resetPullHeader(newsList);
		}
		else
		{
			androidActInd.hide();
		}
        
	}
	
	//Method to call specific api
	function callToApi() 
	{
		loading=true;
		//Start fetching items
		if (p.type == VARS._WP) 
		{
			//Fetch WordPress posts
			API_news.fetchWordPresPosts({
				url : p.url,
				count : p.count,
				page : page,
				listener : setPosts,
				cat : p.cat,
			})
		} 
		else if (p.type == VARS._RSS) 
		{
			//Fetch RSS items
			API_news.fetchRSSArticles({
				url : p.url,
				listener : setPosts,
			})
		} 
		else if (p.type == VARS._TW) 
		{
			//Fetch Twitter tweets
			API_news.fetchTweeterTweet({
				user : p.user,
				listener : setPosts,
			})

		} 
		else if (p.type == VARS._JOOMLA) 
		{
			//Fetch Joomla Articles
			API_news.fetchJoomlaArticles({
				url : p.url,
				count : p.count,
				page : page,
				listener : setPosts,
				cat : p.cat,
			})
		}
		
		
		//Display indicator in android
		if(VARS._platform==VARS._android&&page==1)
		{
			androidActInd.show();
		}
	}
	callToApi();

	
	
	
	//################# EVENT LISTENERS ##############
	//Reload table
	if (VARS._iOS) {
		newsList.addEventListener('scroll', function(e) 
		{
			offset = e.contentOffset.y;
			if (pulling && !reloading && offset > -80 && offset < 0) {
				pulling = false;
				var unrotate = Ti.UI.create2DMatrix();
				imageArrow.animate({
					transform : unrotate,
					duration : 180
				});
				labelStatus.text = L('pulltoreload');
			} 
			else if (!pulling && !reloading && offset < -80) 
			{
				pulling = true;
				var rotate = Ti.UI.create2DMatrix().rotate(180);
				imageArrow.animate({
					transform : rotate,
					duration : 180
				});
				labelStatus.text = L('releasetorefresh');
			}
		});

		function resetPullHeader(table) {
			reloading = false;
			actInd.hide();
			imageArrow.transform = Ti.UI.create2DMatrix();
			imageArrow.show();
			labelStatus.text = L('pulltoreload');
			table.setContentInsets({
				top : 0
			}, {
				animated : true
			});
		}


		newsList.addEventListener('dragEnd', function(e) {
			if (pulling && !reloading && offset < -80) {
				pulling = false;
				reloading = true;
				labelStatus.text = L('updating');
				imageArrow.hide();
				actInd.show();
				e.source.setContentInsets({
					top : 80
				}, {
					animated : true
				});
				page=1;
				callToApi();
			}
		});
	}

	//Click on the table
	newsList.addEventListener('click',function(e)
	{
		if(e.source.rowtype&&e.source.rowtype=='load_more_button')
		{
			//This is click on load more
			if(!loading)
			{
				page++;
				callToApi();
			}
			
		}
		else
		{
			//This is click on element
			if(e.source.data.type==VARS._TW&&VARS._directLinkToTwitter)
			{
				Ti.Platform.openURL(e.source.data.link);
			}
			else
			{
				//if(!p.win.containingTab){p.win.containingTab=}
				p.win.containingTab.open(createArticleDetails(e.source.data));
			}
		}
		
		
	})
	
	
	if (p.type == VARS._JOOMLA || p.type == VARS._WP) {
		if (!p.cat) {
			if (VARS._iOS) {
				p.win.rightNavButton = indicatorButton;
				categorisButton.addEventListener('click', function(e) {
					p.win.containingTab.open(createCategoryList({
						type : p.type,
						url : p.url,
						tab : p.win.containingTab,
					}));
				});
			}

			//In android add menu items
			if (VARS._platform == VARS._android) {
				p.win.activity.onCreateOptionsMenu = function(e) {
					var menu = e.menu;
					var menuItemCategories = menu.add({
						title : L('categories')
					});
					var menuItemReload = menu.add({
						title : L('reload')
					});
					menuItemCategories.addEventListener("click", function(e) {
						(createCategoryList({
								type : p.type,
								url : p.url,
								tab : p.win.containingTab,
							})).open();

					});
					menuItemReload.addEventListener("click", function(e) {
						page=1;
						callToApi();
					});
				};
			}
		}
	}
	else if(VARS._iOS)
	{
		p.win.rightNavButton=refreshButton;
	}

	
	//Add the list to the window
	p.win.add(newsList);
	
	if(VARS._platform==VARS._android)
	{
		p.win.add(androidActInd);
	}
}

/**
 * 
 * @param {Object} pc Parameters for the categories
 */
var createCategoryList=function(pc)
{
	//############### DATA ###################
	var categories=[];
	//############### VIEWS ##################
	var refreshCategoriesButton=Ti.UI.createButton({
		systemButton:Titanium.UI.iPhone.SystemButton.REFRESH
	})
	
	var indicatorCategoriesButton=Ti.UI.createButton({
		systemButton:Titanium.UI.iPhone.SystemButton.SPINNER
	})
	
	var newsCategoriesList=Ti.UI.createTableView({
		top:"0dp",
		width:"100%",
		height:"100%",
		backgroundColor : VARS.windowBackgroundColor,
		separatorColor : VARS.separatoColor,
	})
	
	var categoriesWindow = Ti.UI.createWindow({
	    title:L('categories'),
		backgroundColor : VARS.windowBackgroundColor,
		navBarHidden : false,
		barColor : VARS.barColor,
		orientationModes : [Titanium.UI.PORTRAIT],
		containingTab:pc.tab,
	})
	
	var newsFromCategoryWindow = Ti.UI.createWindow({
	    title:"",
		backgroundColor : VARS.windowBackgroundColor,
		//containingTab:pc.tab,
		navBarHidden : false,
		barColor : VARS.barColor,
		orientationModes : [Titanium.UI.PORTRAIT]
	})
	newsFromCategoryWindow.containingTab=pc.tab;
	
	//############## FUNCTION #################
	var setCategories=function(theCategories)
	{
		categories=[];
		categoriesWindow.rightNavButton=refreshCategoriesButton;
		for(var i=0;i<theCategories.length;i++)
		{
			categories.push(GUI_news.creteSimpleCategoryRow(theCategories[i]));
		}
		newsCategoriesList.setData(categories);
	}
	
	
	callCategoriesApi = function() 
	{
		categoriesWindow.rightNavButton=indicatorCategoriesButton;
		if (pc.type == VARS._WP) {
			//Fetch WordPress categories
			API_news.fetchWordPresCategories({
				url : pc.url,
				listener : setCategories,
			})
		} else if (pc.type == VARS._JOOMLA) {
			//Fetch WordPress categories
			API_news.fetchJoomlaCategories({
				url : pc.url,
				listener : setCategories,
			})

		}
	}
	callCategoriesApi();
	
	
	
	
	//################## EVENT LISTENER ####################
	refreshCategoriesButton.addEventListener('click',function(e){
		callCategoriesApi();
	})
	newsCategoriesList.addEventListener('click',function(e){
		newsFromCategoryWindow.title=e.source.data.title;
		if(e.source.data.type==VARS._WP)
		{
			
			exports.createNewsList({
				type:VARS._WP,
				url:VARS._WordPress_URL,
				count:VARS._count,
				win:newsFromCategoryWindow,
				tab:pc.tab,
				cat:e.source.data.id,
			})
			pc.tab.open(newsFromCategoryWindow);
		}
		else if(e.source.data.type==VARS._JOOMLA)
		{
			exports.createNewsList({
				type:VARS._JOOMLA,
				url:VARS._JOOMLA_URL,
				count:VARS._count,
				win:newsFromCategoryWindow,
				tab:pc.tab,
				cat:e.source.data.id,
			})
			pc.tab.open(newsFromCategoryWindow);
		}
	})
	
	categoriesWindow.add(newsCategoriesList);
	return categoriesWindow;
}


var startFBShare=function()
        {
        	if(Ti.Facebook.loggedIn) 
				{
					//User is loged in, just share the image
					updateStatus();

				} 
				else 
				{
					//We need to login the user for firs time
					Titanium.Facebook.addEventListener('login', updateStatus);
					Titanium.Facebook.authorize();
				}
        }

var updateStatus = function() 
{
	//Check once again if logged in
	if(Ti.Facebook.loggedIn) 
	{
		var data = {
			link: activeArticle.link,
			name: activeArticle.title,
			message: VARS._shareTitle,
		};
		Titanium.Facebook.requestWithGraphPath('me/feed', data, 'POST', showRequestResult);
	}

}

function showRequestResult(e) {
		var s = '';
		if (e.success) {
			s=L('fbshared');
		} else if (e.cancelled) {
			s=L('fbnotshared');
		} else {
			s=L('fbnotshared');
		}
		alert(s);
	}    

function send_mail(p) {
	var emailDialog = Titanium.UI.createEmailDialog();
	if (!emailDialog.isSupported()) {
		Ti.UI.createAlertDialog({
			title : L('error'),
			message : L('emailnotavailable')
		}).show();
		return;
	}
	emailDialog.setSubject(p.title);
	emailDialog.setHtml(true);
	emailDialog.setMessageBody(VARS._shareTitle+": <a href=\""+p.link+"\">"+p.link+"</a>");

	emailDialog.addEventListener('complete', function(e) {
		if (e.result == emailDialog.SENT) {
			if (Ti.Platform.osname != 'android') {
				// android doesn't give us useful result codes.
				// it anyway shows a toast.
				alert(L('messagesend'));
			}
		} else {
			alert(L('messagenotsend'));
		}
	});
	emailDialog.open();
}




/**
 * 
 * @param {Object} article
 */
var createArticleDetails=function(article)
{
	
	//######################## DATA ################
	var articleHTML="<html>";
	var meta_and_style='<link rel="stylesheet" href="http://twitter.github.com/bootstrap/1.4.0/bootstrap.min.css">';
	    meta_and_style+="<style>img { display:block;  max-width: 300px; height:auto;} hr {margin-bottom:0;} h2 {margin-bottom:0;} h5 { margin-bottom:10px;}</style>"
	    meta_and_style+="<style>body { margin: 10px;}</style>";
	    meta_and_style+="<style>pubDate {bisibility:hidden}</style>"
	var title='<h2>'+article.title+'</h2>';
	var date='<h5>'+article.created_at+"</h5>";
	articleHTML=meta_and_style+"<body>"+title;
    if(!article.type==VARS._TW)
	{
		articleHTML+=date+"<hr />";
	}

	
	articleHTML+=article.content;
	
	if(article.type==VARS._TW)
	{
		articleHTML+='<br/> by <a href="https://www.twitter.com/'+article.user+'">'+article.user+'</a>';
	}
	else
	{
		//Add read article 
		articleHTML+='<br /><a href="'+article.link+'">'+L('source')+'</a><br />'
	}
	
	articleHTML+="</body></html>"
	
	//######################## VIEWS ##############
	
	var articleDetailsWindow = Ti.UI.createWindow({
	    title:article.title,
		backgroundColor : VARS.windowBackgroundColor,
		navBarHidden : false,
		barColor : VARS.barColor,
		orientationModes : [Titanium.UI.PORTRAIT]
	})
	articleWebHeight="100%";
	if(VARS.displayAdMobAdsNews)
	{
		var theAd=ADS.requestAd({win:articleDetailsWindow});
		articleWebHeight=VARS._dpiHeight-VARS.AdHeightDisplayed;
	}
	
	var articleWeb = Ti.UI.createWebView({
			top:0,
            height: articleWebHeight,
            width: "100%",
            html: articleHTML,
        });
        
    var shareButton=Ti.UI.createButton({
    	style:Ti.UI.iPhone.SystemButtonStyle,
    	title:L('share'),
    })
    
    var shareDialog=Ti.UI.createOptionDialog({
    	options:[L('facebook'),L('emailshare'),L('cancel')],
    	cancel:2,
    	title:L('share')
    })
    
    shareButton.addEventListener("click",function(e){
    	shareDialog.show();
    })
    
    shareDialog.addEventListener('click',function(e){
    	if(e.index==0)
    	{
    		activeArticle=article;
    		startFBShare();
    	}
    	else if(e.index==1)
    	{
    		send_mail(article);
    	}
    })
        
    articleDetailsWindow.add(articleWeb);
    if(VARS._iOS){
    	articleDetailsWindow.setRightNavButton(shareButton);
    }
   
   //In android add menu items
	if(VARS._platform==VARS._android)
	{
		articleDetailsWindow.activity.onCreateOptionsMenu = function(e){
  			var menu = e.menu;
  			var menuItemShare = menu.add({ title: L('share') });
  			menuItemShare.addEventListener("click", function(e) 
  			{
  					shareDialog.show();
    					
  			});
		};
	}
			
    return articleDetailsWindow;
}
