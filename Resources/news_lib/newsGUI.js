/*
* @author DANIEL DIMOV
* @email dimovdaniel@yahoo.com
* @content Contains GUI creators and default styles
*/

var VARS =require('common/globals');

//######################### COLORS ##################
var tableBackgroundColor="black";

//######################## SIZES ####################
var rowHeight="65dp";
var imageHeight="55dp";
var imageWidth="55dp";

//############# NORMAL NEWS0 ROW CREATOR ##################
var HtmlCodesDecode=function(str)
{
	str=str.replace(/&#8220;/g,'"');
	str=str.replace(/&#8216;/g,'\'');
	str=str.replace(/&#8221;/g,'"');
	str=str.replace(/&#8217;/g,'\'');
	str=str.replace(/&#8211;/g,'-');
	return str;
	
}
exports.creteSimpleRow=function(p)
{
	//The Row
	var bgColor=VARS.rowColor;
	if(p.index%2==1){bgColor=VARS.rowOddColor}
	var row = Ti.UI.createTableViewRow({
		height:rowHeight,
		hasChild:VARS._platform!=VARS._android,
		backgroundColor:bgColor});
	row.className = 'datarow';
	row.clickName = 'row';
	row.index = p.index;
	row.data=p;
	
	//The icon
	var icon=Ti.UI.createImageView({
		image:VARS._imageNewsDefault,
		touchEnabled:false,
		height:imageHeight,
		width:imageWidth,
		left:"5dp",
		top:"5dp",
		borderRadius:5,
	})
	icon.image=p.avatar;
	row.add(icon);
	
	//The title
	var textW=(VARS._dpiWidth-100)+"dp";
	var theTitle = Ti.UI.createLabel({
		text:HtmlCodesDecode(p.title),
		left:"70dp",
		height:"20dp",
		top:"0dp",
		font:VARS.h2,
		width:textW,
		color:VARS.textColor,
		touchEnabled:false,
	})
	row.add(theTitle);
	var textTop="20dp";
	var textHeight="32dp"
	if(VARS._platform==VARS._android){textHeight="27dp"}
	if(p.title=="")
	{
		textTop="0dp";
		textHeight="52dp";
	}
	var theText = Ti.UI.createLabel({
		text:HtmlCodesDecode(p.text),
		value:p.text,
		left:"70dp",
		height:textHeight,
		top:textTop,
		font:VARS.normal,
		width:textW,
		color:VARS.textColor,
		touchEnabled:false,
	})
	row.add(theText);
	
	var theDate = Ti.UI.createLabel({
		text:p.created_at,
		left:"70dp",
		height:"15dp",
		bottom:"0dp",
		font:VARS.empahasys,
		width:textW,
		color:VARS.textSecondColor,
		touchEnabled:false,
	})
	row.add(theDate);
	
	return row;
}


exports.creteSimpleCategoryRow=function(p)
{
	//The Row
	var bgColor=VARS.rowColor;
	//if(p.index%2==1){bgColor=VARS.rowOddColor}
	var row = Ti.UI.createTableViewRow({
		height:"40dp",
		hasChild:VARS._platform!=VARS._android,
		backgroundColor:bgColor});
	row.className = 'datarow';
	row.clickName = 'row';
	row.index = p.index;
	row.data=p;
	
	
	//The left strip
	var strip=Ti.UI.createView({
		top:0,
		left:0,
		width:"5dp",
		height:"40dp",
		touchEnabled:false,
		backgroundColor:VARS.listColors[p.index%VARS.listColors.length],
	})
	row.add(strip);
	
	
	
	//The title
	var textW="220dp";
	if(VARS._platform==VARS._iPad)
	{
		textW="668";
	}
	else if(VARS._platform==VARS._android)
	{
		textW="230dp";
	}
	var theTitle = Ti.UI.createLabel({
		text:HtmlCodesDecode(p.title),
		left:"15dp",
		height:"40dp",
		top:"0dp",
		font:VARS.h2,
		width:textW,
		color:VARS.textColor,
		touchEnabled:false,
	})
	row.add(theTitle);
	
	return row;
}


var smartButton=function(p)
{
	var theButtonBg=Ti.UI.createView(p.conf);
	if(VARS._iOS)
	{
		//iOS
		theButtonBg.backgroundGradient={
			type:'linear',
			colors:[{color:VARS.buttonTopColor,position:0.0},{color:VARS.buttonBottomColor,position:1.0}]
		};
		if(p.bgGard!=null)
		{
			theButtonBg.backgroundGradient=p.bgGard;
		}
	}
	else
	{
		//Android
		theButtonBg.backgroundColor=VARS.buttonColor;
		
	}
	
	theButtonBg.borderRadius=3;
	
	
	
	var theLblInside = Ti.UI.createLabel({
		textAlign : 'center',
		text : p.text,
		color : VARS.textColor,
		width : p.conf.width,
		touchEnabled:false,
	}); 
	theButtonBg.add(theLblInside);
	
	if(p.displayIndicator&&VARS._iOS)
	{
		var actIndLoading=Ti.UI.createActivityIndicator({
			left:"10dp",
			//style:Titanium.UI.iPhone.ActivityIndicatorStyle.DARK,
			width:"30dp",
			height:"30dp",
		})
		theButtonBg.add(actIndLoading);
		actIndLoading.show();
	}
	
	return theButtonBg;
	
}

exports.createLoadMoreButton=function(p)
{
	var bgColor=VARS.rowColor;
	if(p.size%2==1){bgColor=VARS.rowOddColor}
	var row = Ti.UI.createTableViewRow({
		height:"60dp",
		backgroundColor:bgColor,
		});
	row.rowtype = 'load_more_button';
	
	row.add(smartButton({
		text:L('load_more'),
		conf:{
			width:"100dp",
			touchEnabled:false,
			height:"40dp",
			},
	}))
	
	
	row.addEventListener('click', function(e) {
		e.source.add(smartButton({
			text : L('updating'),
			displayIndicator:true,
			conf : {
				width : "200dp",
				touchEnabled : false,
				height : "40dp",
			},
		}))
	})

	return row;
}
