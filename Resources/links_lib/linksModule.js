/*
* @author DANIEL DIMOV
* @email dimovdaniel@yahoo.com
* @content links Module
* @version 1.0
*/
var VARS =require('common/globals');
if(VARS.displayAdMobAdsLink){var ADS=require('common/ads');} //AD LIBRARY
/**
 * Module that creates list of links
 * @param {Object} p
 * @config {Array} links, list of inks to be created
 */
exports.createLinks=function(p)
{
	//AD
	linksTableHeight="100%";
	if(VARS.displayAdMobAdsLink)
	{
		var theAd=ADS.requestAd({win:p.win});
		linksTableHeight=VARS._dpiHeight-VARS.AdHeightDisplayed;
	}	
	var linksList=Ti.UI.createTableView({
		top:"0dp",
		width:"100%",
		height:linksTableHeight,
		backgroundColor : VARS.windowBackgroundColor,
		separatorColor : VARS.separatoColor,
	});
	
	for(var i=0;i<p.links.length;i++)
	{
		var linkRow=Ti.UI.createTableViewRow({
			title:p.links[i].name,
			link:p.links[i].link,
			color:VARS.textColor,
			backgroundColor:VARS.rowOddColor,
			hasChild:true,
			font:VARS.h2,
			height:"50dp"
		})
		if(i%2==0)
		{
			linkRow.backgroundColor=VARS.rowColor;
		}
		linksList.appendRow(linkRow);
	}
	
	linksList.addEventListener('click',function(e){

		linkWindow=Ti.UI.createWindow({
	    			title:e.row.title,
					backgroundColor : VARS.windowBackgroundColor,
					navBarHidden : false,
					barColor : VARS.barColor,
					orientationModes : [Titanium.UI.PORTRAIT]
				})

				var webviewlink = Titanium.UI.createWebView({
					url : e.row.link,
					//pluginState: Titanium.UI.Android.WEBVIEW_PLUGINS_ON,
				});
				linkWindow.add(webviewlink);
				
				VARS.createWebViewButtons(webviewlink,e.row.link);
				
				
				p.win.containingTab.open(linkWindow);
		
	})
	
	p.win.add(linksList);
}

// function createWebViewButtons(webwindow,url){

// 	
	// var current_url = url;
// 	
	// // Loading Indigator 
		 // var loadIndBg = Ti.UI.createView({
		 	// id:"loadIndBg",
        // width:70,
        // height:70,
        // backgroundColor:'#000000',
        // borderRadius:8,
        // visible:false,
        // opacity:0.6
    // });
//         
   // // webwindow.add(loadIndBg);    
//  	
  // var loadInd = Ti.UI.createActivityIndicator({
        // height:50,
        // width:50,
        // style:Ti.UI.iPhone.ActivityIndicatorStyle.BIG
    // }); 
    // //webwindow.add(loadInd);
//     
    // //create toolbar
//     
     // var toolBg = Ti.UI.createView({
     	// bottom:8,
        // width:150,
        // height:40,
        // backgroundColor:'#000000',
        // borderRadius:8,
        // opacity:0.6
    // });
    // webwindow.add(toolBg);     
//  
    // // create a back button image
    // var back_button = Ti.UI.createButton({
      // backgroundImage:'re_l_prev_06.png',
      // enabled:false,
      // bottom:20,
      // left:100,
       // width:14,
        // height:16
     // //   backgroundColor:'#000000'
    // });
    // webwindow.add(back_button);
    // back_button.addEventListener('click', function(){
        // webwindow.goBack();
    // });
//     
    // // create a forward button image
    // var forward_button = Ti.UI.createButton({
       // backgroundImage:'re_l_next_06.png',
      // enabled:false,
      // bottom:20,
      // right:100,
       // width:14,
        // height:16
     // //   backgroundColor:'#000000'
    // });
    // webwindow.add(forward_button);
    // forward_button.addEventListener('click', function(){
        // webwindow.goForward();
    // });
    // var refresh_button = Ti.UI.createButton({
         // backgroundImage:'re_l_03.png',
      // enabled:false,
      // bottom:20,
       // width:14,
        // height:16
     // //   backgroundColor:'#000000'
    // });
    // webwindow.add(refresh_button);
    // refresh_button.addEventListener('click', function(){
        // webwindow.reload();
    // });
//     
//     
     // webwindow.addEventListener('beforeload',function(e){
        // // loadIndBg.opacity = 0.6;
       // //loadInd.opacity = 1;
       // loadIndBg.show();
       // loadIndBg.visible=true;
    // }); 
    // webwindow.addEventListener('load',function(e){
    
//     	
    	// loadIndBg.visible=false;
//     	 
    	 // //loadIndBg.opacity = 0; 
       // //loadInd.opacity = 0;
        // // set the control buttons
        // if(webwindow.canGoForward()){
            // forward_button.enabled = true;
        // } else {            
            // forward_button.enabled = false;
        // }
        // if(webwindow.canGoBack()){
            // back_button.enabled = true;
        // } else {            
            // back_button.enabled = false;
        // }
        // current_url = e.url;
    // });
// 
// 
// }


